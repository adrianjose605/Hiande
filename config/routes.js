/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

//----------Views--------------------------------------
  'GET /login': { view: 'login'},
  'GET /': 'MainController.view',
  'GET /sales': 'ServiceController.sales',
  'GET /sale': 'ServiceController.sale',
  'GET /dashboard': 'ServiceController.dashboard',
  'GET /report': 'ReportController.view',
  'GET /forms': 'FormController.view',
  'GET /ships': 'ShipController.ships',
  'GET /destinations': 'DestinationController.city',
  'GET /signup': 'AuthController.signup',
  'GET /recovery': 'AuthController.recoveryPass',
  'GET /users': 'AuthController.userAll',
  'GET /myaccount': 'AuthController.myaccount',
  'GET /tickets':'TicketController.view' ,
  'GET /profiles': 'AuthController.profileAll',
  'GET /captain': 'CaptainController.view',
  'GET /passengers': 'PassengerController.view',
  'POST /logout': 'AuthController.logout',

//----------API--------------------------------------
  'POST /route/destination/': 'RouteController.saveRouteDest',
  'POST /routeShip/': 'Ship_route_destController.findRouteShip',
  'POST /routeAll/': 'Ship_route_destController.findRouteAll',
  'POST /ShipRouteDest/': 'Ship_route_destController.createorupdate',
  'POST /ShipRouteDest/delete/': 'Ship_route_destController.deleteAll',
  'POST /ShipDates/': 'Ship_datesController.createorupdate',
  'GET /passenger/name/:name': 'PassengerController.findByName',  
  'POST /countticket': 'TicketController.cantticket',
  'POST /reportApi': 'TicketController.report',
  'GET /getShip': 'ShipController.getShip',
  'GET /getUser': 'UserController.getUser',
  'POST /updateAccount': 'UserController.updateAccount',
  'POST /master/insertDest': 'DestinationController.insertDestination'

  
  
  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
