ang.factory('reportAdmonService', ['$http',function($http) {

  var print={
    Print: function (parameter) {
      var base=window.location.protocol+'//'+window.location.hostname;
      var reportUrl = base+":5488/api/report";
      return $http.post(reportUrl, parameter, { responseType: 'arraybuffer' }).then(function (response) {
        return response;
      });
    }
  }; 

  return print;
  
}]);
'use strict';
ang.controller('report',['$scope','$mdDialog','$http','$cookieStore','$sce','reportAdmonService', function($scope,$mdDialog,$http,$cookieStore,$sce,reportAdmonService) {
  $scope.filter={};
  $scope.filter.type_sale = true;
  $scope.filter.ship;
  $scope.filter.route;
   $scope.filter.date_i=new Date();
   $scope.filter.date_f=new Date();
   $scope.filter.date_f.setHours(24,0,0);
   $scope.filter.date_i.setHours(0,0,0);
  $scope.content="";
  $scope.loading=false;

      listShip();
  $scope.filterReport=function(){
   
    $scope.loading=true;
    var data={};
    var contentP={};
   
   
  var token={'headers': {"token": $cookieStore.get('token')}};
  //var url='/report?date_travel>='+new Date($scope.filter.date_i).toISOString();+'&'+'date_travel<='+new Date($scope.filter.date_f).toISOString()+'&status=true';
  //console.log($scope.filter.ship);
  //console.log($scope.filter.route);
  if($scope.filter.route=='undefined') delete $scope.filter.route;
  if($scope.filter.ship=='undefined') delete $scope.filter.ship;
var data={'date_i':new Date($scope.filter.date_i).toISOString(),'date_f':new Date($scope.filter.date_f).toISOString(),'status':$scope.filter.type_sale,'ship':$scope.filter.ship,'route':$scope.filter.route};

$http.post('/reportApi',data,token).then(function(response){
  
    var datI=(new Date($scope.filter.date_i).getDate()<10?'0':'')+""+new Date($scope.filter.date_i).getDate()+"/"+(new Date($scope.filter.date_i).getMonth()<10?'0':'')+""+(+new Date($scope.filter.date_i).getMonth()+1)+"/"+new Date($scope.filter.date_i).getFullYear();
    var datF=(new Date($scope.filter.date_f).getDate()<10?'0':'')+""+new Date($scope.filter.date_f).getDate()+"/"+(new Date($scope.filter.date_f).getMonth()<10?'0':'')+""+(+new Date($scope.filter.date_f).getMonth()+1)+"/"+new Date($scope.filter.date_f).getFullYear();
    var total=0; if(response.data.length>0) total=response.data[response.data.length-1].total;
    //console.log(response);
  contentP={'list':response.data,'date_travel':datI,'date_travel_end':datF,'total':total};

  var parameter = { "template": { "shortid": "ByxHZSeZ7" },"data":contentP};

  reportAdmonService.Print(parameter).then(function (result) {
    var file = new Blob([result.data], { type: 'application/pdf' });

    var fileURL = URL.createObjectURL(file);
    $scope.content = $sce.trustAsResourceUrl(fileURL);
    $scope.loading=false;
    
      });
})
}


$scope.saveIndexS=function(index){

  var token={'headers': {"token": $cookieStore.get('token')}};
  
  $scope.getRoute($scope.ships[index].id);
  
};

$scope.getShip=function (id){

  $scope.barcos=[];
  
  var token={'headers': {"token": $cookieStore.get('token')}};
  $http.post('/routeAll/',{'idroute':id},token).then(function(response){
    for (var i = 0; i < response.data.length; i++) {

      $http.get('/ship?id='+response.data[i].idship,token).then(function(res){
        if(res.data && res.data.status){
          $scope.barcos.push({'name':res.data.name,'id':res.data.id});
        }

      });

    }
  });
}
$scope.getRoute=function (id){

      $scope.routes=[];

      var token={'headers': {"token": $cookieStore.get('token')}};
      $http.post('/routeAll/',{'idship':id},token).then(function(response){
        for (var i = 0; i < response.data.length; i++) {

          $http.get('/route?id='+response.data[i].idroute,token).then(function(res){
            if(res.data && res.data.status){
              $scope.routes.push({'description':res.data.description,'id':res.data.id});
            //  console.log($scope.routes);
            }

          });

        }

      });
    }

      function listShip(){
        $scope.ships=[];
        var token={'headers': {"token": $cookieStore.get('token')}};
        $http.get('/ship',token).then(function(resp){
            $scope.ships=resp.data;
        });
      }



    



  }]);