'use strict';
ang.controller('ships',['$scope','$mdDialog','$http','NgTableParams', '$resource','$cookieStore', function($scope,$mdDialog,$http,NgTableParams,$resource,$cookieStore) {

	$scope.ship=[];

	$scope.init_ship=function(){
		var token={'headers': {"token": $cookieStore.get('token')}};
		$http.get('/getShip',token).then(function(response){
			$scope.ship=response.data;
		});
	}
	$scope.init_ship();
	$scope.saveIndexS=function(index){
		$scope.iL=index;
		var token={'headers': {"token": $cookieStore.get('token')}};
		$scope.shipSel=$scope.ship[index];

	};
	
	$scope.saveShip=function(){
	var token={'headers': {"token": $cookieStore.get('token')}};
		$scope.loading=true;
		
		if($scope.shipSel.status_suit==null || $scope.shipSel.status_suit==false){
			$scope.shipSel.status_suit=false;
			delete $scope.shipSel.cant_suit;
		}
		if($scope.shipSel.status_cabins==null || $scope.shipSel.status_cabins==false){
			$scope.shipSel.status_cabins=false;
			delete $scope.shipSel.cant_cabins;
		}
		if($scope.shipSel.status_generic==null || $scope.shipSel.status_generic==false){
			$scope.shipSel.status_generic=false;
			delete $scope.shipSel.cant_generic;
		}
		$http.put('/ship/'+$scope.shipSel.id,$scope.shipSel,token).then(function(response){
			if(response){
				notify("Exito","success");
				$scope.loading=false;
				
			}
		});
	}
	$scope.showAdvanced = function(ev) {

		$mdDialog.show({
			controller: DialogController,
			templateUrl: 'templates/ship/dialog_newship.ejs',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
  })
		.then(function(variable) {
			$scope.status = 'sesion exitosa';

		}, function() {
			$scope.status = 'You cancelled.';
		});
	};
	function DialogController($scope, $mdDialog,$http,$cookieStore) {
		$scope.newShip={'name':'','status_generic':false,'status_cabins':false,'status_suit':false,'cant_generic':'','cant_cabins':'','cant_suit':''};
		var token={'headers': {"token": $cookieStore.get('token')}};
		$scope.saveShip=function(){
			$scope.loading=true;
	
			if($scope.newShip.status_suit==null || $scope.newShip.status_suit==false){
				$scope.newShip.status_suit=false;
				delete $scope.newShip.cant_suit;
			}
			if($scope.newShip.status_cabins==null || $scope.newShip.status_cabins==false){
				$scope.newShip.status_cabins=false;
				delete $scope.newShip.cant_cabins;
			}
			if($scope.newShip.status_generic==null || $scope.newShip.status_generic==false){
				$scope.newShip.status_generic=false;
				delete $scope.newShip.cant_generic;
			}
			$http.post('/ship',$scope.newShip,token).then(function(response){
				if(response){
					notify("Exito","success");
					$scope.loading=false;
					$mdDialog.hide();
				}
			});
		}
	}

}]);
