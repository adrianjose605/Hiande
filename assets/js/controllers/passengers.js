'use strict';
ang.controller('passengers',['$scope','$mdDialog','$http','NgTableParams', '$resource','$cookieStore', function($scope,$mdDialog,$http,NgTableParams,$resource,$cookieStore) {



  $scope.status=[{'id':true,'title':'Activo'},{'id':false,'title':'Inactivo'}];

  var self = this;
  $scope.user={};
  $scope.init_user=function(){
    var token={'headers': {"token": $cookieStore.get('token')}};

    $http.get('/passenger',token).then(function(response){
      if(response && response.data.length){

        self.tableParams = new NgTableParams({}, { dataset: response.data});
        console.log(response.data);
      }

    });

  }

  $scope.init_user();

  //-------------------Modal Dialog Edit
  $scope.showAdvanced = function(ev, id) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'templates/passenger/passenger_detail.ejs',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen,
      locals: {
        item: {'id':id,'token':{'headers': {"token": $cookieStore.get('token')}}}
      } // Only for -xs, -sm breakpoints.
    })
    .then(function(save) {
      $scope.init_user();
    }, function(cancel) {

    });
  };
  function DialogController($scope, $mdDialog,$http,item) {
   $scope.dataUser = {};
   $scope.loading=false;
   $http.get('/passenger/'+item.id,item.token).then(function(response){
    $scope.dataUser =response.data;
    
  });


   $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.save = function() {
    $scope.loading=true;
    
    
    $http.put('/passenger/'+item.id,$scope.dataUser,item.token).then(function(response){
      notify('Passageiro re-cadastrado','success');
      $mdDialog.hide();  
      $scope.loading=false;
    })
  };
}

//-------------------/Modal Dialog Edit
//-------------------Modal Dialog new
$scope.showDialogNew = function(ev) {
  $mdDialog.show({
    controller: DialogNewController,
    templateUrl: 'templates/passenger/passenger_new.ejs',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: $scope.customFullscreen,
    locals: {
      item: {'token':{'headers': {"token": $cookieStore.get('token')}}}
      } // Only for -xs, -sm breakpoints.
    })
  .then(function(save) {
    $scope.init_user();
  }, function(cancel) {

  });
};
function DialogNewController($scope, $mdDialog,$http,item) {
 $scope.dataNewUser = {};
 $scope.loading=false;
 $scope.profiles=[{'id':1,'description':'Vendedor'},{'id':2,'description':'Administrador'},{'id':3,'description':'Capitan'}];
 
 $scope.hide = function() {
  $mdDialog.hide();
};

$scope.cancel = function() {
  $mdDialog.cancel();
};

$scope.save = function() {
  $scope.loading=true;
  
  $http.post('/passenger/',$scope.dataNewUser,item.token).then(function(response){
    console.log(response);
    notify('User novo success','success');
    $scope.loading=false;
    $mdDialog.hide();  
  })
};
}
//-------------------------/Modal Dialog new



}]);