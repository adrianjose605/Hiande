'use strict';
ang.controller('destination',['$scope','$timeout','$http','$cookieStore', '$q','$timeout','$mdDialog','MaterialCalendarData',function($scope,$timeout,$http, $cookieStore,$q,webNotification,$mdDialog,CalendarData) {
  var self = this;
  $scope.dest=[];
  $scope.dataCostos=[];

    $scope.linhas =[]; //[{description:"Manaus - Uarini",id:0, escalas:[2,5,4,6]},{description:"Manaus - Carauari",id:1, escalas:[1,2,4,6,8]},{description:"Manaus - Tabatinga",id:2, escalas:[6,2,4,8]}];
    $scope.hourstart="00:00";
    $scope.hourend="00:00";
    var datesRoute=[];
    self.readonly = false;
    self.selectedItem = null;
    self.searchText = null;
    self.querySearch = querySearch;
    self.selectedVegetables = [];
    self.numberChips = [];
    self.numberChips2 = [];
    self.numberBuffer = '';
    self.autocompleteDemoRequireMatch = true;
    self.transformChip = transformChip;
    $scope.dest = $scope.dest;
    $scope.aux=-1;
    actRoutes();
    function actRoutes(){

      var token={'headers': {"token": $cookieStore.get('token')}};
      $http.get('/route',token).then(function(response){
        $scope.linhas=response.data;

      });
    };
    actShips();
    function actShips(){

      var token={'headers': {"token": $cookieStore.get('token')}};
      $http.get('/ship',token).then(function(response){
        $scope.ship=response.data;

      });
    }

    function transformChip(chip) {
      // If it is an object, it's already a known chip
      if (angular.isObject(chip)) {
        return chip;
      }

      // Otherwise, create a new one
      return { description: chip, abrvState: 'new' }
    }

    /**
     * Search for vegetables.
     */
     function querySearch (query) {
      var results = query ? $scope.dest.filter(createFilterFor(query)) : [];
      
      return results;
    }

    /**
     * Create filter function for a query string
     */
     function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(vegetable) {
        return (vegetable._lowername.indexOf(lowercaseQuery) === 0) ||
        (vegetable._lowertype.indexOf(lowercaseQuery) === 0);
      };

    }
    loadVegetables();
    function loadVegetables() {
      $scope.loadDestination=true;
      var token={'headers': {"token": $cookieStore.get('token')}};
      $http.get('/destination/',token).then(function(response){
        
        $scope.dest=response.data;
        $scope.loadDestination=false;
        
        $scope.dest.map(function (veg) {
          veg.iddest= veg.id;
          delete veg.id;
          veg._lowername = veg.description.toLowerCase();
          veg._lowertype = veg.abrvstate.toLowerCase();
          return veg;
        });



      });

    }



    $scope.linhaSel="";
    function CalendarBlankRange(){
      for (var i =0; i <datesRoute.length; i++) {
        CalendarData.setDayContent(datesRoute[i],'<img /> ');  
      }
      
      datesRoute=[];
    }
    $scope.selectLinhas=function (index){
      $scope.aux=index;
      $scope.data={};
      $scope.hourstart=""; $scope.hourend="";
     // console.log($scope.linhas[index]);
     $scope.linhaSel=$scope.linhas[index];
     if($scope.shipSel){
      var token={'headers': {"token": $cookieStore.get('token')}};
      $http.post('/routeAll/',{'idroute':$scope.linhaSel.id, 'idship': $scope.shipSel.id},token).then(function(response){
        $scope.escalas=response.data;
        self.selectedVegetables=$scope.escalas;
        if(response.data && response.data[0]){
         $scope.hourstart=response.data[0].time;
         $scope.hourend=response.data[0].time_return;
       }

     });
      CalendarBlankRange();
      
      $http.get('/ship_dates?where={"idship":'+$scope.shipSel.id+', "idroute":'+$scope.linhaSel.id+'}',token).then(function(response){

        if(response.data){

          var obj=[];var j=0;
          for (var i = 0; i < response.data.length; i++) {
            obj[j]=new Date(response.data[i].date);
            obj[j+1]=new Date(response.data[i].date_return);
            j+=2;
            CalendarData.setDayContent(new Date(response.data[i].date),'<img src="images/emis.png" style="width:100%; height:25px;" /> ');  
            CalendarData.setDayContent(new Date(response.data[i].date_return),' <img src="images/return.png" style="width:100%; height:25px;"/> ');  
            datesRoute.push(new Date(response.data[i].date));
            datesRoute.push(new Date(response.data[i].date_return));

          }

          $scope.selectedDate=obj;

        }else{
          notify('Sem cadastros','info')
        }

      });
    }


  }; 
  $scope.selectShip=function (index){
    $scope.data={};
    $scope.shipSel=$scope.ship[index];
    $scope.hourstart=""; $scope.hourend="";
    if($scope.linhaSel){

      var token={'headers': {"token": $cookieStore.get('token')}};
      $http.post('/routeAll/',{'idroute':$scope.linhaSel.id, 'idship': $scope.shipSel.id},token).then(function(response){

        $scope.escalas=response.data;

        self.selectedVegetables=$scope.escalas;
        if(response.data && response.data[0]){
          
        $scope.hourstart=response.data[0].time;
        $scope.hourend=response.data[0].time_return;
        }

      });
      CalendarBlankRange();
      $http.get('/ship_dates?where={"idship":'+$scope.shipSel.id+', "idroute":'+$scope.linhaSel.id+'}',token).then(function(response){

        if(response.data){

          var obj=[];var j=0;
          for (var i = 0; i < response.data.length; i++) {
            obj[j]=new Date(response.data[i].date);
            obj[j+1]=new Date(response.data[i].date_return);
            j+=2;
            CalendarData.setDayContent(new Date(response.data[i].date),'<img src="images/emis.png" style="width:100%; height:25px;" /> ');  
            CalendarData.setDayContent(new Date(response.data[i].date_return),' <img src="images/return.png" style="width:100%; height:25px;"/> ');  
            datesRoute.push(new Date(response.data[i].date));
            datesRoute.push(new Date(response.data[i].date_return));

          }

          $scope.selectedDate=obj;

        }else{
          notify('Sem cadastros','info')
        }

      });
    }

  };
  function refreshData(){
    if($scope.linhaSel && $scope.shipSel){
      $scope.hourstart=""; $scope.hourend="";
      var token={'headers': {"token": $cookieStore.get('token')}};
      $http.post('/routeAll/',{'idroute':$scope.linhaSel.id, 'idship': $scope.shipSel.id},token).then(function(response){
        $scope.escalas=response.data;
        self.selectedVegetables=$scope.escalas;

      });
      CalendarBlankRange();
      $http.get('/ship_dates?where={"idship":'+$scope.shipSel.id+', "idroute":'+$scope.linhaSel.id+'}',token).then(function(response){

        console.log(response);
        if(response.data){

          var obj=[];var j=0;
          for (var i = 0; i < response.data.length; i++) {
            obj[j]=new Date(response.data[i].date);
            obj[j+1]=new Date(response.data[i].date_return);
            j+=2;
            CalendarData.setDayContent(new Date(response.data[i].date),'<img src="images/emis.png" style="width:100%; height:25px;" /> ');  
            CalendarData.setDayContent(new Date(response.data[i].date_return),' <img src="images/return.png" style="width:100%; height:25px;"/> ');  
            datesRoute.push(new Date(response.data[i].date));
            datesRoute.push(new Date(response.data[i].date_return));

          }

          $scope.selectedDate=obj;

        }else{
          notify('Sem cadastros','info')
        }

      });
    }
  }
  $scope.promiseSave=function(objData,i){
    var token={'headers': {"token": $cookieStore.get('token')}};
    var deferred = $q.defer();
    var resp={};
    // for (var i = 0; i < self.selectedVegetables.length; i++) {
     var obj=$scope.dataCostos.find(function(element){
       return element.id==objData.id;
     });
     if(obj){
      $scope.data={'idroute':$scope.linhaSel.id*1,'idship':$scope.shipSel.id*1, 'iddest':objData.iddest*1,'time':$scope.hourstart ,'time_return':$scope.hourend,'nroorden':i+1,'costo_generic':obj.costo_generic,'costo_cabins':obj.costo_cabins,'costo_suit':obj.costo_suit,'status':obj.status};
    }else
    $scope.data={'idroute':$scope.linhaSel.id*1,'idship':$scope.shipSel.id*1,'time':$scope.hourstart ,'time_return':$scope.hourend, 'iddest':objData.iddest*1,'nroorden':i+1};

    $http.post('/ShipRouteDest/', {'data':$scope.data},token).then(function(response){


      if(response.data){
        if(response.data instanceof Array)
          resp={'id':response.data[0].id,'idship':response.data[0].idship,'idroute':response.data[0].idroute};
        else
          resp={'id':response.data.id,'idship':response.data.idship,'idroute':response.data.idroute};

        deferred.resolve(resp);
        
      }else{
       notify('Error, consulte soporte','error');
     }
   });

   // }
   return deferred.promise;
 }

 $scope.data={};
 $scope.saveRoutes=function(){
      //if($scope.aux!=-1){
        var token={'headers': {"token": $cookieStore.get('token')}};
        // var promise = $q.all($scope.promiseSave());
        var promise = []
        var deferred = $q.defer(); 
        var resp={};
        for (var i = 0; i < self.selectedVegetables.length; i++) {
          promise.push($scope.promiseSave(self.selectedVegetables[i],i));
        }
        var fin=$q.all(promise).then((values)=>{

         resp['idship']=values[0].idship;
         resp['idroute']=values[1].idroute;
         
         for (var l = 0; l < values.length; l++){
           resp['id'+l]=values[l].id;
         }
         $http.post('/ShipRouteDest/delete/',{'data':resp,'l':values.length},token).then(function(response){
           if(response)
             notify('Actualizado con exito','success');
         });
       });

      }

      $scope.showNewDestination = function(ev) {

        $mdDialog.show({
          controller: DialogDestinationController,
          templateUrl: 'templates/route/dialog_newdestination.ejs',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
        .then(function(variable) {
          loadVegetables();

        }, function() {
          $scope.status = 'You cancelled.';
        });
      };
      function DialogDestinationController($scope, $mdDialog,$http,$cookieStore) {
        $scope.newDest={'abrvstate':''};
        $scope.states=[{'description':'Amazonas','abrv':'AM'},{'description':'Acre','abrv':'AC'},{'description':'Alagoas','abrv':'AL'},{'description':'Amapá','abrv':'AP'},{'description':'Bahia','abrv':'BA'},{'description':'Ceará','abrv':'CE'},{'description':'Espírito Santo','abrv':'ES'},{'description':'Goiás','abrv':'GO'},{'description':'Maranhao','abrv':'MA'},{'description':'Mato Grosso del Sur','abrv':'MS'},{'description':'Minas de Greais','abrv':'MG'},{'description':'Pará','abrv':'PA'},{'description':'Paraíba','abrv':'PB'},{'description':'Paraná','abrv':'PR'},{'description':'Pernambuco','abrv':'PE'},{'description':'Piuaí','abrv':'PI'},{'description':'Rio de Janeiro','abrv':'RJ'},{'description':'Rio Grande del Norte','abrv':'RN'},{'description':'Rondônia','abrv':'RO'},{'description':'Roraima','abrv':'RO'},{'description':'Santa Catarina','abrv':'SC'},{'description':'Sao Paulo','abrv':'SP'},{'description':'Sergipe','abrv':'SE'},{'description':'Tocantins','abrv':'TO'},{'description':'Distrito Federal','abrv':'DF'}];
        $scope.index=0;
        $scope.selState=function(index){
          $scope.index=index;
          $scope.newDest.abrvstate=$scope.states[index].abrv;

        }

        $scope.saveDest=function(){
         var token={'headers': {"token": $cookieStore.get('token')}};
         $http.post('/destination',$scope.newDest,token).then(function(response){
          if(response.data){
            notify('Exito','success');
          // console.log(response);
          $mdDialog.hide();
          $scope.newDest={'abrvstate':''};
        }
      });
       }

     } 

     $scope.showNewRoute = function(ev) {

      $mdDialog.show({
        controller: DialogRouteController,
        templateUrl: 'templates/route/dialog_newroute.ejs',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
      .then(function(variable) {
        actRoutes();

      }, function() {
        $scope.status = 'You cancelled.';
      });
    };
    function DialogRouteController($scope, $mdDialog,$http,$cookieStore) {
      $scope.newRoute={'status':false};

      $scope.index=0;
      $scope.selType=function(index){
        $scope.index=index;
        $scope.newDest.abrvstate=$scope.states[index].abrv;

      }

      $scope.saveRoute=function(){
       var token={'headers': {"token": $cookieStore.get('token')}};
       $http.post('/route',$scope.newRoute,token).then(function(response){
        if(response.data){
          notify('Exito','success');
        // console.log(response);
        $mdDialog.hide();
        $scope.newRoute={'status':false};

      }
    });
     }

   }

   $scope.showDetailDest = function(ev,chip) {

    $mdDialog.show({
      controller: DialogDetailController,
      templateUrl: 'templates/route/dialog_detaildest.ejs',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen,
      locals: {
        item: {'chip':chip,'idship':$scope.shipSel.id*1,'idroute':$scope.linhaSel.id*1,'token':{'headers': {"token": $cookieStore.get('token')}}}
      }  // Only for -xs, -sm breakpoints.
    })
    .then(function(variable) {
      $scope.dataCostos.push({'id':variable.id,'costo_generic':variable.generic*1,'costo_cabins':variable.cabins*1,'costo_suit':variable.suit*1,'status':variable.status});

    // $scope.saveRoutes();
  }, function() {

  });
  };
  function DialogDetailController($scope, $mdDialog,$http,item) {

   $scope.detDest={};
   $scope.name=item.chip.description;
   if(item.chip.id)
    var url='/ship_route_dest/'+item.chip.id;
  else
    var url='/ship_route_dest?iddest='+item.chip.iddest+'&idroute='+item.idroute+'&idship='+item.idship;


  $http.get(url,item.token).then(function(response){
    if(response.data){
      $http.get('/ship/'+item.idship,item.token).then(function(resp){
        if(resp.data){
          $scope.perms={'generic':resp.data.status_generic,'cabins':resp.data.status_cabins,'suit':resp.data.status_suit};    
          $scope.detDest={'id':item.chip.id,'generic':response.data.costo_generic,'cabins':response.data.costo_cabins,'suit':response.data.costo_suit,'status':response.data.status};
        }

      });
    }
  });

  $scope.saveDetDest=function(){
    $mdDialog.hide($scope.detDest);
    notify('Cadastre suas modificaciones','info');

  }

}

//---------------TAB 2
function CalendarBlank(dateArray){
 for (var i =0; i <dateArray.length; i++) {
  CalendarData.setDayContent(new Date(dateArray[i]),'<img /> ');  
}
}
$scope.selectedDate = [];
$scope.dayClick = function(date) {

  var find=$scope.selectedDate.find(function(elemnt){
    //console.log(elemnt);
    return new Date(elemnt).getTime()==new Date(date).getTime();
  });
  if(!find){
    CalendarBlank([date]);
  }else if($scope.selectedDate.length!=1 && new Date(date).getTime()<new Date($scope.selectedDate[$scope.selectedDate.length-2]).getTime()){
    $scope.selectedDate.pop();
    notify('fecha de retorno invalida','warning');
  }else if(($scope.selectedDate.length-1)%2==0){
    CalendarData.setDayContent(new Date(date),'<img src="images/emis.png" style="width:100%; height:25px;" /> ');  
  }else 
  CalendarData.setDayContent(new Date(date),' <img src="images/return.png" style="width:100%; height:25px;"/> ');


  
};

$scope.saveDates=function(){

  if ($scope.selectedDate.length%2==0){
    var token={'headers': {"token": $cookieStore.get('token')}};

    // var hend=(new Date($scope.hourend).getHours()<10?'0':'') +new Date($scope.hourend).getHours()+":"+(new Date($scope.hourend).getMinutes()<10?'0':'') + new Date($scope.hourend).getMinutes();
    // var hstart=(new Date($scope.hourstart).getHours()<10?'0':'') +new Date($scope.hourstart).getHours()+":"+(new Date($scope.hourstart).getMinutes()<10?'0':'') + new Date($scope.hourstart).getMinutes();
    
    for (var i = 0; i < $scope.selectedDate.length; i+=2) {
      var start;
      var end;
      start=$scope.selectedDate[i];
      end=$scope.selectedDate[i+1];
      $http.post('/ShipDates/',{'data':{'idship':$scope.shipSel.id*1,'idroute':$scope.linhaSel.id*1,'date':start,'date_return':end}},token).then(function(response){

        if(response.data){
          notify('Cadastro exitoso','success');
        }

      });


    }
  }else
  notify('Complete itinerario salida-llegada','info');



}
////------------------------TAB 3---------------
// $(document).ready(function () {
//   var chart= new Highcharts.chart('divChart', {
//    chart: {
//     plotBackgroundColor: null,
//     plotBorderWidth: null,
//     plotShadow: false,
//     type: 'pie'
//   },
//   title: {
//     text: 'Vendas'
//   },
//   tooltip: {
//     pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
//   },
//   plotOptions: {
//     pie: {
//       allowPointSelect: true,
//       cursor: 'pointer',
//       dataLabels: {
//         enabled: true,
//         format: '<b>{point.name}</b>: {point.percentage:.1f} %',
//         style: {
//           color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
//         }
//       }
//     }
//   },
//   series: [{
//     name: 'Brands',
//     colorByPoint: true,
//     data: [{
//       name: 'Vendido',
//       y: 256
//     }, {
//       name: 'Disponible',
//       y: 444
//     }]
//   }]
// });
//   $(window).resize(function() {
//     var height = chart.height;
//     var width = $("#chartContent").width();
//     chart.setSize(width, height);
//   });
// });

}]);