'use strict';
ang.controller('users',['$scope','$mdDialog','$http','NgTableParams', '$resource','$cookieStore', function($scope,$mdDialog,$http,NgTableParams,$resource,$cookieStore) {



  $scope.status=[{'id':true,'title':'Activo'},{'id':false,'title':'Inactivo'}];

  var self = this;
  $scope.user={};
  $scope.init_user=function(){
    var token={'headers': {"token": $cookieStore.get('token')}};

    $http.get('/user',token).then(function(response){
      if(response && response.data.length){
        console.log(response);
        self.tableParams = new NgTableParams({}, { dataset: response.data});
      }

    });

  }

  $scope.init_user();

  //-------------------Modal Dialog Edit
  $scope.showAdvanced = function(ev, id) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'templates/user/user_detail.ejs',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen,
      locals: {
        item: {'id':id,'token':{'headers': {"token": $cookieStore.get('token')}}}
      } // Only for -xs, -sm breakpoints.
    })
    .then(function(save) {
      $scope.init_user();
    }, function(cancel) {

    });
  };
  function DialogController($scope, $mdDialog,$http,item) {
   $scope.dataUser = {};
   $scope.loading=false;
   $http.get('/user/?id='+item.id,item.token).then(function(response){
    $scope.dataUser =response.data;
    $scope.dataUser.password="000hiande000";
  });


   $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.save = function() {
    $scope.loading=true;
    if($scope.dataUser.password=='000hiande000'){
      delete $scope.dataUser.password;
    }
    if($scope.dataUser.idprofile.id)
      $scope.dataUser.idprofile=$scope.dataUser.idprofile.id;


    $http.put('/user/'+item.id,$scope.dataUser,item.token).then(function(response){
      notify('User novo success','success');
      // console.log(response);
      $scope.loading=false;
      $mdDialog.hide();  
    })
  };
}

//-------------------/Modal Dialog Edit
//-------------------Modal Dialog new
$scope.showDialogNew = function(ev) {
  $mdDialog.show({
    controller: DialogNewController,
    templateUrl: 'templates/user/user_new.ejs',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: $scope.customFullscreen,
    locals: {
      item: {'token':{'headers': {"token": $cookieStore.get('token')}}}
      } // Only for -xs, -sm breakpoints.
    })
  .then(function(save) {
    $scope.init_user();
  }, function(cancel) {

  });
};
function DialogNewController($scope, $mdDialog,$http,item) {
 $scope.dataNewUser = {};
 $scope.loading=false;
 $scope.company=[];
 $scope.profiles=[{'id':1,'description':'Vendedor'},{'id':2,'description':'Administrador'},{'id':3,'description':'Capitan'}];
 $http.get('/profile/',item.token).then((response)=>{
  if(response.data)
    $scope.profiles=response.data;
});

 $http.get('/company/',item.token).then((response)=>{
  if(response.data)
    $scope.company=response.data;
});

 $scope.hide = function() {
  $mdDialog.hide();
};

$scope.cancel = function() {
  $mdDialog.cancel();
};

$scope.save = function() {
  $scope.loading=true;

  $http.post('/user/',$scope.dataNewUser,item.token).then(function(response){
    console.log(response);
    notify('User novo success','success');
    $scope.loading=false;
    $mdDialog.hide();  
  })
};
}
//-------------------------/Modal Dialog new



}]);