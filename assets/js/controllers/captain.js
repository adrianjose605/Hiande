'use strict';
ang.factory('listReportService', ['$http',function($http) {

  var print={
    Print: function (parameter) {
      var base=window.location.protocol+'//'+window.location.hostname;
      var reportUrl = base+":5488/api/report";
      return $http.post(reportUrl, parameter, { responseType: 'arraybuffer' }).then(function (response) {
        return response;
      });
    }
  }; 

  return print;
  
}]);
ang.controller('captain',['$scope','$timeout','$http','$cookieStore', '$q','$timeout','$mdDialog','MaterialCalendarData','$sce','listReportService',function($scope,$timeout,$http, $cookieStore,$q,webNotification,$mdDialog,CalendarData,$sce,listReportService) {
  $scope.labels = ["Total Vendidos", "Disponível"];
  $scope.max={'generic':300,'cabins':2,'suit':2};
  
  $scope.sold={'generic':0,'cabins':0,'suit':0};
  $scope.total_sold_g=$scope.max.generic-$scope.sold.generic;
  $scope.total_sold_c=$scope.max.cabins-$scope.sold.cabins;
  $scope.total_sold_s=$scope.max.suit-$scope.sold.suit;

  $scope.data1 = [$scope.sold.generic, $scope.total_sold_g];
  $scope.data2 = [$scope.sold.cabins, $scope.total_sold_c];
  $scope.data3 = [$scope.sold.suit, $scope.total_sold_s];
  $scope.statusSale_g=false;
  $scope.statusSale_c=false;
  $scope.statusSale_s=false;
  

  $scope.act_chart=function(obj){
    $scope.sold={'generic':obj.cant_g,'cabins':obj.cant_c,'suit':obj.cant_s};
    $scope.total_sold_g=$scope.max.generic-$scope.sold.generic;
    $scope.data1 = [$scope.sold.generic, $scope.total_sold_g];
    $scope.total_sold_c=$scope.max.cabins-$scope.sold.cabins;
    $scope.data2 = [$scope.sold.cabins, $scope.total_sold_c];
    $scope.total_sold_s=$scope.max.suit-$scope.sold.suit;
    $scope.data3 = [$scope.sold.suit, $scope.total_sold_s];
  } 
  
  $scope.dataCostos=[];
  $scope.linhas =[]; 
  $scope.selectedIndex=0;
  var datesRoute=[];
  $scope.aux=-1;

  //-----------List
  actRoutes();
  function actRoutes(){

    var token={'headers': {"token": $cookieStore.get('token')}};
    $http.get('/route',token).then(function(response){
      $scope.linhas=response.data;

    });
  };
  actShips();
  function actShips(){

    //console.log($("#profileI").val());
    var token={'headers': {"token": $cookieStore.get('token')}};
    $http.get('/getShip/',token).then(function(response){
      $scope.ship=response.data;
      

    });
  }
  //-----------/List
    //--------TABS
    $scope.next=function(){
      if($scope.linhaSel.id && $scope.shipSel.id && $scope.dateSelected)
        $scope.selectedIndex=1;
      else
        notify('Seleccione Barco, Linha é Data','info');
    }; 
    $scope.prev=function(){
      $scope.selectedIndex=0;
    };
    //--------/TABS

    
    $scope.linhaSel="";
    function CalendarBlankRange(){
      for (var i =0; i <datesRoute.length; i++) {
        CalendarData.setDayContent(datesRoute[i],'<img /> ');  
      }
      
      datesRoute=[];
    }
    $scope.selectLinhas=function (index){
      $scope.aux=index;


      $scope.linhaSel=$scope.linhas[index];
      if($scope.shipSel){
        var token={'headers': {"token": $cookieStore.get('token')}};

        CalendarBlankRange();

        $http.get('/ship_dates?where={"idship":'+$scope.shipSel.id+', "idroute":'+$scope.linhaSel.id+'}',token).then(function(response){

          if(response.data){

            var obj=[];var j=0;
            for (var i = 0; i < response.data.length; i++) {
              obj[j]=new Date(response.data[i].date);
              obj[j+1]=new Date(response.data[i].date_return);
              j+=2;
              if(new Date(response.data[i].date) < new Date())  {
                CalendarData.setDayContent(new Date(response.data[i].date),'<img src="images/start_old.png" style="width:100%; height:25px;" /> ');  
                CalendarData.setDayContent(new Date(response.data[i].date_return),' <img src="images/end_old.png" style="width:100%; height:25px;"/> ');  
              }else{
                CalendarData.setDayContent(new Date(response.data[i].date),'<img src="images/emis.png" style="width:100%; height:25px;" /> ');  
                CalendarData.setDayContent(new Date(response.data[i].date_return),' <img src="images/return.png" style="width:100%; height:25px;"/> ');  
              }

              datesRoute.push(new Date(response.data[i].date));
              datesRoute.push(new Date(response.data[i].date_return));
              DatesStart.push(new Date(response.data[i].date));
            }

            $scope.selectedDate=obj;

          }else{
            notify('Sem cadastros','info')
          }

        });
      }


    }; 
    var DatesStart=[];
    $scope.selectShip=function (index){

      $scope.shipSel=$scope.ship[index];

      if($scope.linhaSel){

        var token={'headers': {"token": $cookieStore.get('token')}};

        CalendarBlankRange();
        $http.get('/ship_dates?where={"idship":'+$scope.shipSel.id+', "idroute":'+$scope.linhaSel.id+'}',token).then(function(response){

          if(response.data){

            var obj=[];var j=0;
            for (var i = 0; i < response.data.length; i++) {
              obj[j]=new Date(response.data[i].date);
              obj[j+1]=new Date(response.data[i].date_return);
              j+=2;
              if(new Date(response.data[i].date) < new Date())  {
                CalendarData.setDayContent(new Date(response.data[i].date),'<img src="images/start_old.png" style="width:100%; height:25px;" /> ');  
                CalendarData.setDayContent(new Date(response.data[i].date_return),' <img src="images/end_old.png" style="width:100%; height:25px;"/> ');  
              }else{
                CalendarData.setDayContent(new Date(response.data[i].date),'<img src="images/emis.png" style="width:100%; height:25px;" /> ');  
                CalendarData.setDayContent(new Date(response.data[i].date_return),' <img src="images/return.png" style="width:100%; height:25px;"/> ');  
              }
              datesRoute.push(new Date(response.data[i].date));
              DatesStart.push(new Date(response.data[i].date));
              datesRoute.push(new Date(response.data[i].date_return));
            }

            $scope.selectedDate=obj;

          }else{
            notify('Sem cadastros','info')
          }

        });
      }

    };
    $scope.getDetailShip=function (idship,idroute){

      var token={'headers': {"token": $cookieStore.get('token')}};
      var url='/ship/'+idship;
      $http.get(url,token).then(function(response){

        if(response.data ){
          $scope.max={'generic':response.data.cant_generic,'cabins':response.data.cant_cabins,'suit':response.data.cant_suit};
          $scope.status={'suit':response.data.status_suit,'cabins':response.data.status_cabins,'generic':response.data.status_generic}

        }else{
         $scope.max={'generic':0,'cabins':0,'suit':0};
         $scope.status={'suit':'','cabins':'','generic':''};
       }

     });

    }
    function CalendarBlank(dateArray){
     for (var i =0; i <dateArray.length; i++) {
      CalendarData.setDayContent(new Date(dateArray[i]),'<img /> ');  
    }
  }
  $scope.selectedDate = [];
  $scope.dayClick = function(date) {
    $scope.statusSale_g=false;
    $scope.statusSale_c=false;
    $scope.statusSale_s=false;
    var find=DatesStart.find(function(elemnt){

      return new Date(elemnt).getTime()==new Date(date).getTime();
    });

    if(find){
      $scope.dateSelected=date;
      notify('Ok','success');
      var token={'headers': {"token": $cookieStore.get('token')}};
      $scope.getDetailShip($scope.shipSel.id,$scope.linhaSel.id);
      var data={'idship':$scope.shipSel.id,'idroute':$scope.linhaSel.id,'date_travel':date};

      io.socket.on("cantSold"+data.idroute+""+data.idship+""+new Date($scope.dateSelected).toISOString(), function (data){
       $scope.$apply(function () {
         $scope.act_chart(data);             

       });
     });

      $http.post('/countticket',data,token).then(function(response){
       if(response.data){
        $scope.act_chart(response.data);
        $scope.next();
      }else
      notify('Error, consulte soporte','error');
    });
      $http.get('/ship_dates?idship='+$scope.shipSel.id+'&idroute='+$scope.linhaSel.id+'&date='+new Date($scope.dateSelected).toISOString(),token).then(function(response){
        console.log(response);
        if(response.data && response.data[0]){          
          $scope.statusSale_g=angular.copy(response.data[0].status_g);
          $scope.statusSale_c=angular.copy(response.data[0].status_c);
          $scope.statusSale_s=angular.copy(response.data[0].status_s);
        }
        console.log(response.data[0].status_g);
        
      });
    }else{
      notify('Data invalida','warning');
    }
  };
  $scope.showListPassenger = function(ev) {

    $mdDialog.show({
      controller: DialogTicketController,
      templateUrl: 'templates/report/dialog_listPassenger.ejs',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen,
          locals:{'item':{'content':$scope.content}} // Only for -xs, -sm breakpoints.
        })
    .then(function(variable) {


    }, function() {

    });
  };
  function DialogTicketController($scope, $mdDialog,$http,item) {
    $scope.content=item.content;
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

  }
  $scope.printList=function(type){


    if($scope.dateSelected && $scope.shipSel.id && $scope.linhaSel.id){
      if (type==1) $scope.loading1=true;
      if (type==2) $scope.loading2=true;
      if (type==3) $scope.loading3=true;
      var token={'headers': {"token": $cookieStore.get('token')}};
      $http.get('/ticket?idroute='+$scope.linhaSel.id+'&idship='+$scope.shipSel.id+'&date_travel='+new Date($scope.dateSelected).toISOString()+'&status=true&type_passagem='+type,token).then(function(response){

        if(response.data && response.data.length>0){
          var arr=[];
          for (var i = 0; i < response.data.length; i++) {
            arr.push({'id':i+1,'name':response.data[i].idpassenger.name,'cpf':response.data[i].idpassenger.cpf,'identified':response.data[i].idpassenger.identified,'phone':response.data[i].idpassenger.phone,'cod_ticket':response.data[i].cod_ticket});
          }
          var obj={};
          obj.ship=response.data[0].idship.name;
          obj.route=response.data[0].idroute.description;
          var options = { year: 'numeric', month: 'short', day: 'numeric' };         
          obj.date_travel=new Date(response.data[0].date_travel).toLocaleDateString('pt-BR', options);
          obj.date_travel_end=new Date(response.data[0].date_travel_end).toLocaleDateString('pt-BR', options);
          obj.list=arr;
          var parameter = { "template": { "shortid": "ryPr-Hl-7" },"data":obj};
          listReportService.Print(parameter).then(function (result) {

            var file = new Blob([result.data], { type: 'application/pdf' });
            var fileURL = URL.createObjectURL(file);
            $scope.content = $sce.trustAsResourceUrl(fileURL);
            if (type==1) $scope.loading1=false;
            if (type==2) $scope.loading2=false;
            if (type==3) $scope.loading3=false;
            // console.log(window.innerWidth);
            // console.log(window.innerHeight);
            if(window.innerWidth<400 ){
             window.open($scope.content,'_blank').focus();
            
           }else
           $scope.showListPassenger();


         });
        }else{
          notify("Sem Bilhetes vendidos","warn");
        }
      });
    }
  }

  $scope.stopSales=function(type){

    var token={'headers': {"token": $cookieStore.get('token')}};

    $http.get('/ship_dates?idroute='+$scope.linhaSel.id+'&idship='+$scope.shipSel.id+'&date='+new Date($scope.dateSelected).toISOString(),token).then(function(resp){

      if(resp.data && resp.data[0]){
        var data={};
        if(type==1){
          $scope.statusSale_g=$scope.statusSale_g==false;
          data.status_g=$scope.statusSale_g; 
        }
        if(type==2){
          $scope.statusSale_c=$scope.statusSale_c==false;
          data.status_c=$scope.statusSale_c;
        } 
        if(type==3){
          $scope.statusSale_s=$scope.statusSale_s==false;
          data.status_s=$scope.statusSale_s;
        } 
        console.log(data);
        $http.put('/ship_dates/'+resp.data[0].id,data,token).then(function(response){
          if(response){

            if(type==1) if(data.status_g) notify('Vendas OK','info'); else notify('Stop Vendas','warn');          
            if(type==2) if(data.status_c) notify('Vendas OK','info'); else notify('Stop Vendas','warn');
            if(type==3) if(data.status_s) notify('Vendas OK','info'); else notify('Stop Vendas','warn');

          }
        });
      }
    });

  }
}]);
