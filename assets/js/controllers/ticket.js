'use strict';
ang.factory('reportService', ['$http',function($http) {

  var print={
    Print: function (parameter) {
      var base=window.location.protocol+'//'+window.location.hostname;
      // var base="https://hiande.jsreportonline.net";
      var reportUrl = base+":5488/api/report";
      var token={};
      return $http.post(reportUrl, parameter, { responseType: 'arraybuffer','headers': {"access_token": '13e0f8c0-6ffd-11e8-809c-d53bc34a26fe'}}).then(function (response) {
        return response;
      });
    }
  }; 

  return print;
  
}]);
var colorR='ui-state-highlight';
var colorV='ui-state-highlight-v';
$(document).ready(
  function(){ 
    var colorR='ui-state-highlight';
    var colorV='ui-state-highlight-v';

    var date2 = new Date();
    $('#calendario').multiDatesPicker({},colorV); 
    $('#calendario_ret').multiDatesPicker({},colorR); 
  });
ang.controller('ticket',['$scope','$mdDialog','$http','$cookieStore','$filter','MaterialCalendarData','$sce','reportService','NgTableParams', '$resource','$timeout',function($scope,$mdDialog,$http,$cookieStore,$filter,CalendarData,$sce,reportService,NgTableParams,$resource,$timeout) {
 $scope.bil = {linha:""};
 $scope.linhas=[];
 $scope.barcos=[] ;
 $scope.iL='';
 $scope.iB='';
 $scope.user={'name':'','cpf':'','email':'','phone':'','address':'', 'birthdate':''};
 $scope.escalas=[];
 $scope.bil.detail_ship=1;
 $scope.selectedIndex=0;
///--------------------------

$scope.preliminar={origin:{'id':'','name':''},destination:{'id':'','name':''},type:{'cost':'','pref':'','class':''},date:{'ini':'','end':''}};
var date = new Date();

$scope.dayFormat = "d";
$scope.selectedDate = null;

$scope.selectedDate = false;

$scope.firstDayOfWeek = 0;
$scope.setDirection = function(direction) {
  $scope.direction = direction;
  $scope.dayFormat = direction === "vertical" ? "EEEE, MMMM d" : "d";
};

$scope.disp_ticket={};

$scope.dayClick = function(date) {
  // console.log(new Date());
  // console.log(new Date(date).toISOString());
  $scope.msg = "Dia de saída " + $filter("date")(date, "MMMM d,");
  var indexFound;
  var found= $scope.ship_start.find(function(element,index) {
    indexFound=index;    
    return element == new Date(date).toISOString();
  });

  if(found && (new Date(date)> new Date()) ){

    $scope.bil.date=date;
    $scope.bil.date_end=$scope.ship_end[indexFound];
    $scope.preliminar.date.ini=date;
    $scope.preliminar.date.end=$scope.ship_end[indexFound];
    notify($scope.msg,'info');
      //---------consulta disponibilidad

      var token={'headers': {"token": $cookieStore.get('token')}};
      $http.get('/Ship_dates?idroute='+$scope.bil.route+'&idship='+$scope.bil.ship+'&date_return='+ $scope.bil.date_end,token).then(function(response){
        if(response.data)
          $scope.disp_ticket={'cabins':response.data[0].disp_cabins,'generic':response.data[0].disp_generic,'suit':response.data[0].disp_suit};

      });
      
      $scope.getDetailShip($scope.bil.ship,$scope.bil.route);
      

      $http.post('/countticket',{'idship':$scope.bil.ship,'idroute':$scope.bil.route,'date_travel':date},token).then(function(response){
       if(response.data){
        $scope.disp_ticket={'cabins':$scope.cant.cabin-response.data.cant_c,'generic':$scope.cant.lugar-response.data.cant_g,'suit':$scope.cant.suit-response.data.cant_s};
      }
    });
      io.socket.on("cantSold"+$scope.bil.route+""+$scope.bil.ship+""+date, function (data){
       $scope.$apply(function () {
         $scope.disp_ticket={'cabins':$scope.cant.cabin-data.cant_c,'generic':$scope.cant.lugar-data.cant_g,'suit':$scope.cant.suit-data.cant_s};
       });
     });


    } else{

      notify('Selecione uma data de saída correta','error');
    }

  };

  $scope.prevMonth = function(data) {
    $scope.msg = "You clicked (prev) month " + data.month + ", " + data.year;
  };

  $scope.nextMonth = function(data) {
    $scope.msg = "You clicked (next) month " + data.month + ", " + data.year;
  };

  $scope.tooltips = true;
  $scope.setDayContent=[];



  $scope.st=[{'id':true,'title':'Activo'},{'id':false,'title':'Inactivo'}];

  var self = this;
  $scope.user={};
  $scope.init_ticket=function(){
    var token={'headers': {"token": $cookieStore.get('token')}};

    $http.get('/ticket?sort=id%20DESC',token).then(function(response){
      if(response && response.data.length){

        self.tableParams = new NgTableParams({}, { dataset: response.data});
      }else       self.tableParams = new NgTableParams({}, { dataset: []});

    });

  }


  $scope.saveIndexL=function(index){
    $scope.iL=index;
    var token={'headers': {"token": $cookieStore.get('token')}};
    $scope.escalas=$scope.linhas[index].destinations;
    $scope.getShip($scope.linhas[index].id);
    $scope.disp_ticket={};


  };

  $scope.saveIndexB=function(index){

    $scope.iB=index;
    $scope.getDestination($scope.barcos[index].id,$scope.linhas[$scope.iL].id);
    $scope.getDetailShip($scope.barcos[index].id,$scope.linhas[$scope.iL].id);
    $scope.getDates($scope.barcos[index].id,$scope.linhas[$scope.iL].id);
    $scope.disp_ticket={};

  };
  $scope.CalendarBlank=function(arrayStart,arrayEnd){
   for (var i =0; i <arrayStart.length; i++) {
    CalendarData.setDayContent(new Date(arrayStart[i]),'<img /> ');  
  }for (var i =0; i <arrayEnd.length; i++) {
    CalendarData.setDayContent(new Date(arrayEnd[i]),'<img /> ');  
  }
}
$scope.ship_start=[];
$scope.ship_end=[];
$scope.getDates=function(idship,idroute){

 var date_start=[];
 var date_end=[];
 var token={'headers': {"token": $cookieStore.get('token')}};
 $scope.CalendarBlank($scope.ship_start,$scope.ship_end);
 $scope.ship_start=[];
 $scope.ship_end=[];
 $http.get('/ship_dates?where={"idship":'+idship+', "idroute":'+idroute+'}',token).then(function(response){

  if(response.data && response.data.length)
    for (var i =0; i <response.data.length; i++) {

      $scope.ship_start.push(response.data[i].date);
      $scope.ship_end.push(response.data[i].date_return);
      // console.log(response.data[i].date);
      if(new Date(response.data[i].date) < new Date()){
        CalendarData.setDayContent(new Date(response.data[i].date),'<img src="images/start_old.png" style="width:100%; height:25px;" /> ');  
        CalendarData.setDayContent(new Date(response.data[i].date_return),' <img src="images/end_old.png" style="width:100%; height:25px;"/> ');  
      }else{
        CalendarData.setDayContent(new Date(response.data[i].date),'<img src="images/emis.png" style="width:100%; height:25px;" /> ');  
        CalendarData.setDayContent(new Date(response.data[i].date_return),' <img src="images/return.png" style="width:100%; height:25px;"/> ');  
      }
      

    }else{
     $scope.CalendarBlank($scope.ship_start,$scope.ship_end);
    //-----------------------------------BORRAR------------------------------
  }

});

}
$scope.costos={};
$scope.getDestination=function (idship,idroute){

 var token={'headers': {"token": $cookieStore.get('token')}};
 $http.post('/routeAll/',{'idroute':idroute, 'idship':idship},token).then(function(response){
  $scope.escalas=response.data;
  console.log(response.data);
  if(response.data){    
    $scope.preliminar.origin.id=response.data[0].iddest;
    $scope.preliminar.date.time=response.data[0].time;
    $scope.preliminar.date.time_return=response.data[0].time_return;
    $scope.preliminar.origin.name=response.data[0].description;
    $scope.preliminar.destination.id=response.data[response.data.length-1].iddest;
    $scope.preliminar.destination.name=response.data[response.data.length-1].description;
    $scope.costos.generic=response.data[response.data.length-1].generic;
    $scope.costos.cabins=response.data[response.data.length-1].cabins;
    $scope.costos.suit=response.data[response.data.length-1].suit;
    $scope.bil.iddestinationorigin=response.data[0].iddest;
    $scope.bil.iddestinationend=response.data[response.data.length-1].iddest;
    $scope.pre();
  }

});

}
$scope.cost, $scope.cant, $scope.status={};
$scope.getDetailShip=function (idship,idroute){

  var token={'headers': {"token": $cookieStore.get('token')}};
  var url='/ship/'+idship;
  $http.get(url,token).then(function(response){

    if(response.data ){


      $scope.cant={'suit':response.data.cant_suit,'cabin':response.data.cant_cabins,'lugar':response.data.cant_generic}
      $scope.status={'suit':response.data.status_suit,'cabin':response.data.status_cabins,'lugar':response.data.status_generic}
      
    }else{

     $scope.cant={'suit':'','cabin':'','lugar':''}
     $scope.status={'suit':'','cabin':'','lugar':''}
     
   }

 });

}


$scope.pre=function(){
  if($scope.bil.detail_ship==3){
  //  $scope.preliminar.type.cost=$scope.cost.suit;
  $scope.preliminar.type.cost=$scope.costos.suit;
  $scope.preliminar.type.class="Suit";
}
if($scope.bil.detail_ship==2){
    //$scope.preliminar.type.cost=$scope.cost.cabin;
    $scope.preliminar.type.cost=$scope.costos.cabins;
    $scope.preliminar.type.class="Cabina";
  }
  if($scope.bil.detail_ship==1){
    //$scope.preliminar.type.cost=$scope.cost.lugar;
    $scope.preliminar.type.cost=$scope.costos.generic;
    $scope.preliminar.type.class="Economica";
  }
};

$scope.getShip=function (id){

  $scope.barcos=[];
  $scope.bil.ship="";
  var token={'headers': {"token": $cookieStore.get('token')}};
  $http.post('/routeAll/',{'idroute':id},token).then(function(response){
    for (var i = 0; i < response.data.length; i++) {

      $http.get('/ship?id='+response.data[i].idship,token).then(function(res){
        if(res.data && res.data.status){
          $scope.barcos.push({'name':res.data.name,'id':res.data.id});
        }
      //console.log(res);
    });

    }

  });
}

$scope.now=new Date();

$scope.passengers=[];

$scope.statusTicket=function(id,index){
  var token={'headers': {"token": $cookieStore.get('token')}};

  $http.post('/ticket/'+id,{'status':self.tableParams.data[index].status},token).then(function(response){
    if(response.statusText=="OK");
    notify("Billete actualizado","success");
  });
}
actRoutes();
function actRoutes(){
  $scope.linhas=[];
  var token={'headers': {"token": $cookieStore.get('token')}};
      //$http.get('/ship_route_dest?groupby=idRoute ASC',token).then(function(response){
        $scope.loadRoute=true;
        $http.post('/routeAll/',{'idship':''},token).then(function(response){
          if(response.data.length>0)
           for (var i = 0; i < response.data.length; i++) {
             $http.get('/route?id='+response.data[i].idroute,token).then(function(resp){
              $scope.linhas.push(resp.data);
              $scope.loadRoute=false;
            });
           }
           else  $scope.loadRoute=false;


         });
        
      }


      $scope.qrySearch=function(){
        var token={'headers': {"token": $cookieStore.get('token')}};
        if($scope.user.name)
          return $http.get('/passenger/name/'+$scope.user.name,token).then(function(data){        
           $scope.passengers=data.data;


         });
        else return [];
      };
      var userAux={};
      $scope.selectPassenger=function(id){
        if(id<0){
          userAux={};
          $scope.user={};          
        }else{

          $scope.user={'id':$scope.passengers[id].id,'name':$scope.passengers[id].name,'cpf':$scope.passengers[id].cpf,
          'identified':$scope.passengers[id].identified,'email':$scope.passengers[id].email,'phone':$scope.passengers[id].phone,
          'address':$scope.passengers[id].address, 'birthdate':new Date($scope.passengers[id].birthdate)};
          userAux = angular.copy($scope.user);
          
        }
      }

      $scope.showTicket = function(ev) {
        //console.log(id_T);
        $mdDialog.show({
          controller: DialogTicketController,
          templateUrl: 'templates/ticket/dialog_ticket.ejs',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: $scope.customFullscreen,
          locals:{'item':{'content':$scope.content}} // Only for -xs, -sm breakpoints.
        })
        .then(function(variable) {


        }, function() {

        });
      };
      function DialogTicketController($scope, $mdDialog,$http,item) {
        var token={'headers': {"token": $cookieStore.get('token')}};
        $scope.content=item.content;
        $scope.statusTicket=true;
        $scope.hide = function() {
          $mdDialog.hide();
        };
        
        $scope.cancel = function() {
          $mdDialog.cancel();
        };

      }

      var id_T=null;
      $scope.viewTicket=function(id){
        id_T=null;
        id_T=id;
        var token={'headers': {"token": $cookieStore.get('token')}};
        $http.get('/ticket/'+id,token).then(function(data){
          //console.log(data);

          var options = { weekday: 'long', year: '2-digit', month: 'short', day: 'numeric' };
          data.data.date_travel=new Date(data.data.date_travel).toLocaleDateString('pt-BR', options);
          data.data.date_travel_end=new Date(data.data.date_travel_end).toLocaleDateString('pt-BR', options);
          data.data.idpassenger.birthdate=new Date(data.data.idpassenger.birthdate).toLocaleDateString('pt-BR', { year: 'numeric', month: 'short', day: 'numeric' });
          var parameter = { "template": { "shortid": "BkQ4ZreZm" },"data":data.data,access_token:'13e0f8c0-6ffd-11e8-809c-d53bc34a26fe'};
          reportService.Print(parameter).then(function (result) {
            console.log(result);
            var file = new Blob([result.data], { type: 'application/pdf' });
            
            var fileURL = URL.createObjectURL(file);
            
            $scope.content = $sce.trustAsResourceUrl(fileURL);
            $scope.showTicket();
            // window.open($scope.content,'_blank').focus();


          });

        });
      };
      $scope.getChipInfo=function(obj){

        $scope.preliminar.destination.id=obj.iddest;
        $scope.preliminar.destination.name=obj.description;
        var objRes=$scope.escalas.find(function(element,index) {

          return element.iddest == obj.iddest;
        });
        // console.log(objRes);
        $scope.bil.iddestinationend=objRes.iddest;
        $scope.costos.generic=objRes.generic;
        $scope.costos.cabins=objRes.cabins;
        $scope.costos.suit=objRes.suit;
        $scope.pre();
      }

      $scope.showConfirm = function(ev) {
        var token={'headers': {"token": $cookieStore.get('token')}};
        if( (($scope.user.cpf && $scope.validarCPF()) || $scope.user.identified) && $scope.user.name && $scope.user.address && $scope.user.birthdate && $scope.user.phone){

          if(JSON.stringify($scope.user)==JSON.stringify(userAux)){
            var url ='/passenger/'+$scope.user.id;


            $http.get(url,token).then(function(data){

              if(data.data){
              //preliminar.type.class
              //---------------------------------
              var confirm = $mdDialog.confirm()
              .title('Confirma novo Bilhete?')
              .textContent('Você deseja criar um novo bilhete?')
              .ariaLabel('Bom Dia')
              .targetEvent(ev)
              .ok('Si')
              .cancel('Cancel');
              $mdDialog.show(confirm).then(function() {
                $scope.bil.detail_ship=Number($scope.bil.detail_ship);
                var data= {'idship':$scope.bil.ship,'idroute':$scope.bil.route,'idpassenger':$scope.user.id,'iddestinationorigin':$scope.bil.iddestinationorigin,'iddestinationend':$scope.bil.iddestinationend,'type_passagem':$scope.bil.detail_ship,'date_travel':$scope.bil.date,'date_travel_end':$scope.bil.date_end,'time_travel':$scope.preliminar.date.time,'time_travel_end':$scope.preliminar.date.time_return,'coste':$scope.preliminar.type.cost,'cost_total':$scope.preliminar.type.cost};

                $http.post('/ticket',data,token).then(function(data){

                  notify("Exito",'success','');

                  $scope.viewTicket(data.data.id)
                  // $scope.init_ticket();

                },function(err){
                  notify("Error, Lugares Agotados",'error');
                  
                });

              }, function() {

              });
              //---------------------------------

               //----------enviar post de pasaje----
             }else{

             }

           });
          }else{
            //-----------no equals object
            
            if(userAux.name && (userAux.identified || userAux.cpf)){
              var text='A pessoa: '+$scope.user.name+' é actualizada';

              var confirm = $mdDialog.confirm()
              .title(text)
              .textContent('Você deseja actualizar passageiro')
              .ariaLabel('Bom Dia')
              .targetEvent(ev)
              .ok('Ok')
              .cancel('Cancel');

              $mdDialog.show(confirm).then(function() {

                var data=$scope.user;
                
                $http.put('/passenger/'+data.id,data,token).then(function(data){
                  notify('Usuario editado!','success');
                  $scope.qrySearch();
                  userAux = angular.copy($scope.user);
                  $scope.showConfirm();
                  
                },function(err){
                  if(err){
                    notify('Consulte soporte 456');
                  }   
                });  

              }, function() {

              });
            }else{
            //----crear
            var text='A pessoa: '+$scope.user.name+' é nova';
            var confirm = $mdDialog.confirm()
            .title(text)
            .textContent('Você deseja crear passageiro')
            .ariaLabel('Bom Dia')
            .targetEvent(ev)
            .ok('Ok')
            .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {

              var dataP=$scope.user;   

              $http.post('/passenger/',dataP,token).then(function(dataR){
                if(dataR){

                  $scope.user.id = dataR.data.id;
                  userAux = angular.copy($scope.user);
                  $scope.qrySearch();
                  $scope.showConfirm();
                  notify('Usuario Novo!','success');
                }
              },function(err){
                if(err){
                  notify('Consulte soporte');
                }   
              });  
              
            }, function() {

            });
          }
        }
      }else{
        if(!$scope.user.cpf && !$scope.user.identified){
          notify("Complete CPF o Identificacion",'warn','','cpf');
        }
        if(!$scope.user.name){
          notify("Complete nome",'warn','','na');
        }
        if(!$scope.user.address){
          notify("Complete direção",'warn','','add');
        }
        if(!$scope.user.birthdate){
          notify("Complete data de nascimiento",'warn','','bd');
        }
        if(!$scope.user.phone){
          notify("Complete telefono",'warn','','ph');
        }

      }

    };

    $scope.restoreTicket=function(){

     userAux={};
     $scope.user={'name':'','cpf':'','email':'','phone':'','address':'', 'birthdate':''};

   }
   $scope.next=function(){
    if($scope.bil.ship && $scope.bil.detail_ship && $scope.bil.route && $scope.bil.date)
      $scope.selectedIndex=1;

    else{

      if(!$scope.bil.route)
        notify("Selecione um Destino",'warn','','ro');
      if(!$scope.bil.ship)
        notify("Selecione um Barco",'warn','','sh');
      if(!$scope.bil.detail_ship)
        notify("Elija o Tipo de bilhete",'warn','','dt');
      if(!$scope.bil.date)
        notify("Selecione a data de partida",'warn', { position:"right"},'da');
    } 
  };



///-----------------CPF

$scope.validarCPF=function(boolAlert=true){
  if($scope.user.cpf){
    var cpf = $scope.user.cpf.toString();
    cpf = cpf.replace( "." , "" );
    cpf = cpf.replace( "-" , "" );
    cpf = cpf.replace( "." , "" );
    if (cpf.length != 11 ||
      cpf == "00000000000" ||
      cpf == "11111111111" ||
      cpf == "22222222222" ||
      cpf == "33333333333" ||
      cpf == "44444444444" ||
      cpf == "55555555555" ||
      cpf == "66666666666" ||
      cpf == "77777777777" ||
      cpf == "88888888888" ||
      cpf == "99999999999"){
     if(boolAlert) notify('CPF Invalido, Digite o CPF no formato nnn.nnn.nnn-nn', 'error','','cpf');
   return false;
 } else {
  var sum = 0;
  sum = sum + (parseInt(cpf.substring( 0 , 1))) * 10;
  sum = sum + (parseInt(cpf.substring( 1 , 2))) * 9;
  sum = sum + (parseInt(cpf.substring( 2 , 3))) * 8;
  sum = sum + (parseInt(cpf.substring( 3 , 4))) * 7;
  sum = sum + (parseInt(cpf.substring( 4 , 5))) * 6;
  sum = sum + (parseInt(cpf.substring( 5 , 6))) * 5;
  sum = sum + (parseInt(cpf.substring( 6 , 7))) * 4;
  sum = sum + (parseInt(cpf.substring( 7 , 8))) * 3;
  sum = sum + (parseInt(cpf.substring( 8 , 9))) * 2;
}
var resto1 = (sum * 10) % 11;
if ((resto1 == 10) || (resto1 == 11)) {
  resto1 = 0;
}
var sum = 0;
sum = sum + (parseInt(cpf.substring( 0 , 1))) * 11;
sum = sum + (parseInt(cpf.substring( 1 , 2))) * 10;
sum = sum + (parseInt(cpf.substring( 2 , 3))) * 9;
sum = sum + (parseInt(cpf.substring( 3 , 4))) * 8;
sum = sum + (parseInt(cpf.substring( 4 , 5))) * 7;
sum = sum + (parseInt(cpf.substring( 5 , 6))) * 6;
sum = sum + (parseInt(cpf.substring( 6 , 7))) * 5;
sum = sum + (parseInt(cpf.substring( 7 , 8))) * 4;
sum = sum + (parseInt(cpf.substring( 8 , 9))) * 3;
sum = sum + (parseInt(cpf.substring( 9 , 10))) * 2;

var resto2 = (sum *10) % 11;
if ((resto2 == 10) || (resto2 == 11)) {
  resto2 = 0;
}

if ( (resto1 == (parseInt(cpf.substring( 9 , 10)))) &&  (resto2 == (parseInt(cpf.substring( 10 , 11)))) ) {
  return true;
} else {
 if(boolAlert) notify( 'CPF Invalido, Digite o CPF no formato nnn.nnn.nnn-nn' , 'error','','cpf');

 return false;
}


}else{
  return false
}


}




}]);

