'use strict';
ang.controller('profiles',['$scope','$mdDialog','$http','NgTableParams', '$resource','$cookieStore', function($scope,$mdDialog,$http,NgTableParams,$resource,$cookieStore) {



$scope.status=[{'id':true,'title':'Activo'},{'id':false,'title':'Inactivo'}];

  var self = this;
  $scope.profile={};
  $scope.init_profile=function(){
    var token={'headers': {"token": $cookieStore.get('token')}};

    $http.get('/profile',token).then(function(response){
      if(response && response.data.length){

        self.tableParams = new NgTableParams({}, { dataset: response.data});
      }

    });

  }

  $scope.init_profile();

  //-------------------Modal Dialog Edit
  $scope.showAdvanced = function(ev, id) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'templates/profile/profile_detail.ejs',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen,
      locals: {
        item: {'id':id,'token':{'headers': {"token": $cookieStore.get('token')}}}
      } // Only for -xs, -sm breakpoints.
    })
    .then(function(save) {
      $scope.init_profile();
    }, function(cancel) {

    });
  };
  function DialogController($scope, $mdDialog,$http,item) {
   $scope.dataProfile = {};
   $scope.loading=false;
   $http.get('/profile/?id='+item.id,item.token).then(function(response){
    $scope.dataProfile =response.data;
    $scope.dataProfile.password="000hiande000";
  });


   $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.save = function() {
    $scope.loading=true;
   
       // console.log($scope.dataProfile);
    $http.put('/profile/'+item.id,$scope.dataProfile,item.token).then(function(response){
      notify('profile success','success');
      // console.log(response);
      $scope.loading=false;
      $mdDialog.hide();  
    })
  };
}

//-------------------/Modal Dialog Edit
//-------------------Modal Dialog new
$scope.showDialogNew = function(ev) {
    $mdDialog.show({
      controller: DialogNewController,
      templateUrl: 'templates/profile/profile_new.ejs',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen,
      locals: {
        item: {'token':{'headers': {"token": $cookieStore.get('token')}}}
      } // Only for -xs, -sm breakpoints.
    })
    .then(function(save) {
      $scope.init_profile();
    }, function(cancel) {

    });
  };
  function DialogNewController($scope, $mdDialog,$http,item) {
   $scope.dataNewProfile = {};
   $scope.loading=false;
   
   $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.save = function() {
    $scope.loading=true;
        
    $http.post('/profile/',$scope.dataNewProfile,item.token).then(function(response){
      console.log(response);
      notify('profile novo success','success');
      $scope.loading=false;
      $mdDialog.hide();  
    })
  };
}
//-------------------------/Modal Dialog new



}]);