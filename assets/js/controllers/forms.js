'use strict';
ang.factory('reportAdmonService', ['$http',function($http) {

  var print={
    Print: function (parameter) {
      var base=window.location.protocol+'//'+window.location.hostname;
      var reportUrl = base+":5488/api/report";
      return $http.post(reportUrl, parameter, { responseType: 'arraybuffer' }).then(function (response) {
        return response;
      });
    }
  }; 

  return print;
  
}]);
ang.controller('forms',['$scope','$mdDialog','$http','$cookieStore','$sce','reportAdmonService', function($scope,$mdDialog,$http,$cookieStore,$sce,reportAdmonService) {
  $scope.filter={};
  $scope.filter.type_sale = 'Vendas';
  $scope.filter.ship = true;
  $scope.formDscto={};
 
  $scope.content="";
  $scope.loading=false;


$scope.restoreForm=function(){
$scope.formDscto={};
}
$scope.validarCPF=function(boolAlert=true){
  if($scope.formDscto.id_cpf){
    var cpf = $scope.formDscto.id_cpf.toString();
    cpf = cpf.replace( "." , "" );
    cpf = cpf.replace( "-" , "" );
    cpf = cpf.replace( "." , "" );
    if (cpf.length != 11 ||
      cpf == "00000000000" ||
      cpf == "11111111111" ||
      cpf == "22222222222" ||
      cpf == "33333333333" ||
      cpf == "44444444444" ||
      cpf == "55555555555" ||
      cpf == "66666666666" ||
      cpf == "77777777777" ||
      cpf == "88888888888" ||
      cpf == "99999999999"){
     if(boolAlert) notify('CPF Invalido, Digite o CPF no formato nnn.nnn.nnn-nn', 'error','','id_cpf');
   return false;
 } else {
  var sum = 0;
  sum = sum + (parseInt(cpf.substring( 0 , 1))) * 10;
  sum = sum + (parseInt(cpf.substring( 1 , 2))) * 9;
  sum = sum + (parseInt(cpf.substring( 2 , 3))) * 8;
  sum = sum + (parseInt(cpf.substring( 3 , 4))) * 7;
  sum = sum + (parseInt(cpf.substring( 4 , 5))) * 6;
  sum = sum + (parseInt(cpf.substring( 5 , 6))) * 5;
  sum = sum + (parseInt(cpf.substring( 6 , 7))) * 4;
  sum = sum + (parseInt(cpf.substring( 7 , 8))) * 3;
  sum = sum + (parseInt(cpf.substring( 8 , 9))) * 2;
}
var resto1 = (sum * 10) % 11;
if ((resto1 == 10) || (resto1 == 11)) {
  resto1 = 0;
}
var sum = 0;
sum = sum + (parseInt(cpf.substring( 0 , 1))) * 11;
sum = sum + (parseInt(cpf.substring( 1 , 2))) * 10;
sum = sum + (parseInt(cpf.substring( 2 , 3))) * 9;
sum = sum + (parseInt(cpf.substring( 3 , 4))) * 8;
sum = sum + (parseInt(cpf.substring( 4 , 5))) * 7;
sum = sum + (parseInt(cpf.substring( 5 , 6))) * 6;
sum = sum + (parseInt(cpf.substring( 6 , 7))) * 5;
sum = sum + (parseInt(cpf.substring( 7 , 8))) * 4;
sum = sum + (parseInt(cpf.substring( 8 , 9))) * 3;
sum = sum + (parseInt(cpf.substring( 9 , 10))) * 2;

var resto2 = (sum *10) % 11;
if ((resto2 == 10) || (resto2 == 11)) {
  resto2 = 0;
}

if ( (resto1 == (parseInt(cpf.substring( 9 , 10)))) &&  (resto2 == (parseInt(cpf.substring( 10 , 11)))) ) {
  return true;
} else {
 if(boolAlert) notify( 'CPF Invalido, Digite o CPF no formato nnn.nnn.nnn-nn' , 'error','','id_cpf');

 return false;
}


}else{
  return false
}


}

$scope.showConfirm = function(ev) {
        var token={'headers': {"token": $cookieStore.get('token')}};
        if( (($scope.formDscto.id_cpf && $scope.validarCPF()) || $scope.formDscto.identified) && $scope.formDscto.id_name && $scope.formDscto.id_address && $scope.formDscto.id_matricula && $scope.formDscto.id_phone && $scope.formDscto.id_identified && $scope.formDscto.id_lot  && $scope.formDscto.id_email ){
var confirm = $mdDialog.confirm()
              .title('Confirma novo Cadastro?')
              .textContent('Você deseja criar um novo cadastro?')
              .ariaLabel('Bom Dia')
              .targetEvent(ev)
              .ok('Si')
              .cancel('Cancel');
              $mdDialog.show(confirm).then(function() {
                //$scope.bil.detail_ship=Number($scope.bil.detail_ship);
                //var data= {'idship':$scope.bil.ship,'idroute':$scope.bil.route,'idpassenger':$scope.user.id,'iddestinationorigin':$scope.bil.iddestinationorigin,'iddestinationend':$scope.bil.iddestinationend,'type_passagem':$scope.bil.detail_ship,'date_travel':$scope.bil.date,'date_travel_end':$scope.bil.date_end,'time_travel':$scope.preliminar.date.time,'time_travel_end':$scope.preliminar.date.time_return,'coste':$scope.preliminar.type.cost,'cost_total':$scope.preliminar.type.cost};

                $http.post('/forms',$scope.formDscto,token).then(function(data){
                  notify("Exito",'success','');

                  $scope.viewTicket(data.data.id)
                  // $scope.init_ticket();

                },function(err){
                  notify("Error, Lugares Agotados",'error');
                  
                });

              }, function() {

              });
        }else{
                    notify("Los campos marcados con * son obligatorios",'warn');
        }
}

      listShip();
  $scope.filterReport=function(){

    $scope.loading=true;
    var data={};
    var contentP={};

  var token={'headers': {"token": $cookieStore.get('token')}};
  //var url='/report?date_travel>='+new Date($scope.filter.date_i).toISOString();+'&'+'date_travel<='+new Date($scope.filter.date_f).toISOString()+'&status=true';
var data={'date_i':new Date($scope.filter.date_i).toISOString(),'date_f':new Date($scope.filter.date_f).toISOString(),'status':true};
$http.post('/reportApi',data,token).then(function(response){
  console.log(response);
  
  contentP={'list':response.data,date_travel:$scope.filter.date_i,date_travel_end:$scope.filter.date_f};

var parameter = { "template": { "shortid": "r1YU-SlZ7" },"data":contentP};

  reportAdmonService.Print(parameter).then(function (result) {
    var file = new Blob([result.data], { type: 'application/pdf' });

    var fileURL = URL.createObjectURL(file);

    $scope.content = $sce.trustAsResourceUrl(fileURL);
    
    $scope.loading=false;
      });
})
}


$scope.saveIndexS=function(index){

  var token={'headers': {"token": $cookieStore.get('token')}};
  
  $scope.getRoute($scope.ships[index].id);
  
};

$scope.getShip=function (id){

  $scope.barcos=[];
  
  var token={'headers': {"token": $cookieStore.get('token')}};
  $http.post('/routeAll/',{'idroute':id},token).then(function(response){
    for (var i = 0; i < response.data.length; i++) {

      $http.get('/ship?id='+response.data[i].idship,token).then(function(res){
        if(res.data && res.data.status){
          $scope.barcos.push({'name':res.data.name,'id':res.data.id});
        }

      });

    }
  });
}
$scope.getRoute=function (id){

      $scope.routes=[];

      var token={'headers': {"token": $cookieStore.get('token')}};
      $http.post('/routeAll/',{'idship':id},token).then(function(response){
        for (var i = 0; i < response.data.length; i++) {

          $http.get('/route?id='+response.data[i].idroute,token).then(function(res){
            if(res.data && res.data.status){
              $scope.routes.push({'description':res.data.description,'id':res.data.id});
              console.log($scope.routes);
            }

          });

        }

      });
    }

      function listShip(){
        $scope.ships=[];
        var token={'headers': {"token": $cookieStore.get('token')}};
        $http.get('/ship',token).then(function(resp){
            $scope.ships=resp.data;
        });
      }



    



  }]);