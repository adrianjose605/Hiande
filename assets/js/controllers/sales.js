'use strict';
ang.controller('sales',['$scope','$mdDialog','$http', function($scope,$mdDialog,$http) {
 $scope.status = '  ';
 $scope.login = {user:"",pass:""};
 $scope.customFullscreen = false;
 $scope.loadTicket=false;
 $scope.showAdvanced = function(ev) {
  $mdDialog.show({
    controller: DialogController,
    templateUrl: 'dialog.login.ejs',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:false,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
  .then(function(variable) {
    $scope.status = 'sesion exitosa';

  }, function() {
    $scope.status = 'You cancelled.';
  });
};

function DialogController($scope, $mdDialog) {
 $scope.response="";
 $scope.login = {user:"",pass:""};
 $scope.hide = function() {
  $mdDialog.hide();
};

$scope.cancel = function() {
  $mdDialog.cancel();
};

$scope.login = function(variable) {
 if($scope.login.user && $scope.login.pass)
  $scope.response="";
var data=JSON.stringify($scope.login);

$http.post('/log', data).then(function(response){
  if(response.status && response.status==200){
    $scope.response="Bem-Vindo "+$scope.login.user;
    window.location.assign('/');
  }
});	
};
}
}]);