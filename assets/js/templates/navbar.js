'use strict';
  
 ang.controller('navbar',['$scope', '$mdSidenav', '$log', '$timeout','$http','$cookieStore', function($scope, $mdSidenav, $log, $timeout,$http,$cookieStore) {
  $scope.title = 'SIGESTUR';
  $scope.name = $cookieStore.get('name');
  $scope.log=false;
  $scope.toggleLeft = buildDelayedToggler('left');

   function buildDelayedToggler(navID) {
      return debounce(function() {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav(navID)
          .toggle();
      //     .then(function () {
      //       $log.debug("toggle " + navID + " is done");
      //     });
      }, 200);
    }
     function debounce(func, wait, context) {
      var timer;
      return function debounced() {
        var context = $scope,
            args = Array.prototype.slice.call(arguments);
        $timeout.cancel(timer);
        timer = $timeout(function() {
          timer = undefined;
          func.apply(context, args);
        }, wait || 10);
      };
    };



     $scope.logout = function(variable) {
       
          $http.post('/logout').then(function(response){
             if(response.err){
              $scope.response="Error logout";  
            }else if(response==true){
            }    
                 window.location.assign('/login');
             // $cookieStore.remove('token');
             // $cookieStore.remove('username');
             // $cookieStore.remove('userId');
             
          }); 
     
    };

}]);