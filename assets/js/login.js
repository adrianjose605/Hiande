'use strict';
ang.controller('login',['$scope','$mdDialog','$http','$cookieStore', function($scope,$mdDialog,$http,$cookieStore) {
 $scope.status = '  ';
 $scope.customFullscreen = false;
 $scope.loading=false;
 $scope.showAdvanced = function(ev) {
  if($cookieStore.get('token'))
    window.location.assign('/');


  $mdDialog.show({
    controller: DialogController,
    templateUrl: 'dialog.login.ejs',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:false,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
  .then(function(variable) {
    $scope.status = 'sesion exitosa';

    $scope.ShowRecovery();

  }, function() {
    //---------cancel
    console.log("cancel");
  });
};

function DialogController($scope, $mdDialog) {
 $scope.response="";

 $scope.login = {username:"",password:""};
 $scope.hide=function(){
  $mdDialog.hide();
}
$scope.log = function(variable) {
 if($scope.login.username && $scope.login.password){
   $scope.loading=true;
   $scope.response="";
   var data=JSON.stringify($scope.login);
   $http.post('/auth/index',data).then(function(response){
    if(response.err){
     $scope.loading=false;
     $scope.response="Verifique usuario y password";  
   }else if(response.status && response.status==200){

    if (response.data.user.status) {
      $scope.response="Bem-Vindo "+$scope.login.username;
      $cookieStore.put('token',response.data.token);
          //$cookieStore.put('userId',response.data.user.id);
          $cookieStore.put('name',response.data.user.name);
          //$cookieStore.put('permission',response.data.user.idprofile);
          window.location.assign('/');
          $scope.loading=false;
        }else if(response.err && response.err=='invalid username or password'){
          $scope.loading=false;
          $scope.response="Error ";
        }
        $scope.loading=false;
      }
    },function(response) {

      if(response.data.err)
        $scope.response = "Error: Revise Senha o Username";
      notify("Error al fazer login",'error');
      $scope.loading=false;
    }); 
 }
};
}
//-----------------recover
$scope.ShowRecovery=function(ev){
  
  $mdDialog.show({
    controller: DialogRecoverController,
    templateUrl: 'templates/user/user_recoveryPass.ejs',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:false,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
  .then(function(variable) {
    console.log("login");
   $scope.showAdvanced();

  }, function() {
   $scope.showAdvanced();
 });
};
function DialogRecoverController($scope, $mdDialog) {
  $scope.hide=function(){
    $mdDialog.hide();
  }
  $scope.rec={};
  $scope.recover=function(){
    var data={'name':'adrian','to':$scope.rec.email};
    $http.post('/email/',data).then(function(response){
        
      if(response.data && response.data.err)
        notify(response.data.err,'success');
      else if(response.data)
        notify('Revise su correo','success');

      },function(err){
        
        if(err.data){
          notify(err.data.err,'error');
        }
      });
   
  }
};
//-----------------recover
}]);