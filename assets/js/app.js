'use strict';

angular.module('hiande', ['ngMaterial','ui.bootstrap','ngCookies','ngRoute','angular-web-notification','materialCalendar','ngTable','ngResource','chart.js']);
var ang=angular.module('hiande', ['ngMaterial','ui.bootstrap','ngCookies','ngRoute','angular-web-notification','materialCalendar','ngTable','ngResource','chart.js'])
.config(function($mdThemingProvider) {

  $mdThemingProvider.theme('default')
  .primaryPalette('deep-purple', {
      'default': '700', // by default use shade 400 from the pink palette for primary intentions
      'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
      'hue-2': '700', // use shade 600 for the <code>md-hue-2</code> class
      'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
    })
    // If you specify less than all of the keys, it will inherit from the
    // default shades
    .accentPalette('deep-orange', {
      'default': '700' // use shade 200 for default, and keep all other shades the same
    });

  }).controller('appCtrl', ['$scope', function($scope){
    
    $scope.navigateTo = function(url) {
      window.location=(url);
    };
    
  }]);



function notify(msg,type,opc,parent){
  if(parent)
  $("#"+parent).notify(msg, type,opc);
    else
  $.notify(msg, type,opc);
}