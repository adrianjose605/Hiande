/**
 * Ship_route_destController
 *
 * @description :: Server-side logic for managing ship_route_dests
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

 	findRouteShip:function (req, res){


 		var ship=req.param('idship');
 		//return res.json(req.param());
 		Ship_route_dest.find({ 'idRoute':ship }).populate('idDest')
 		.exec(function(err, ship_route_dest) {
 			//sails.log(ship_route_dest);
 			return res.json({ship_route_dest});
 		});

 	},

 	findRouteAll:function(req,res){

 		var ship=req.param('idship');
 		var route=req.param('idroute');

 		if (ship && route){
 			
 			var obj=new Array(route,ship);
 			Ship_route_dest.findDestination_RS(obj,function (err, ship_route_dest) {
 				if (err) {
 					res.send(400);
 				} else {
 					res.send(ship_route_dest);
 					
 				}
 			});

 		}else if(ship){
 			sails.log("route");
 			 Ship_route_dest.findShip_S(ship,function (err, ship_route_dest) {
 				if (err) {
 					res.send(400);
 				} else {
 					res.send(ship_route_dest);
 				}
 			});

 		}else if(route){
 			
 			Ship_route_dest.findShip_R(route,function (err, ship_route_dest) {
 				if (err) {
 					res.send(400);
 				} else {
 					res.send(ship_route_dest);
 				}
 			});

 		}else
 		Ship_route_dest.findAllRoute(ship,function (err, ship_route_dest) {
 			if (err) {
 				res.send(400);
 			} else {
 				res.send(ship_route_dest);
 			}
 		});

 		// Ship_route_dest.findShip_R(ship,function (err, ship_route_dest) {
 		// 	if (err) {
 		// 		res.send(400);
 		// 	} else {
 		// 		res.send(ship_route_dest);
 		// 	}
 		// });




 	},
 	createorupdate:function(req,res){
 		var data=req.param('data');
 		

 		Ship_route_dest.createOrUpdate(data,function (err, ship_route_dest) {
 		
 				if (err) {
 					res.send(400);
 				} else {
 					res.send(ship_route_dest);
 					
 				}
 			});
 	},
 	deleteAll:function(req,res){
 		var data=[req.param('data'),req.param('l')]; 		

 		Ship_route_dest.destroy_res(data,function (err, ship_route_dest) {
 		
 				if (err) {
 					res.send(400);
 				} else {
 					res.send(ship_route_dest);
 					
 				}
 			});
 	}
 };

