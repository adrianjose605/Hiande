/**
 * EmailController
 *
 * @description :: Server-side logic for managing emails
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

 	create: function (req, res) {
 		var email = req.body;

 		email.token_unique='';
 		var randtoken = require('rand-token');
 		var token = randtoken.generate(24);
 		email.token_unique=token;
 		var valid=new Date();
 		valid.setDate(valid.getDate() + 1);
 		
 		email.valid=valid;
 		
 		User.findOne({email: email.to}, function (err, user) {
 			if (!user) {
 				return res.json(401, {err: 'invalid email'});
 			}
 			if(user.status){
 				Email.create(email).exec(function(err, records){
 					if(err)
 						return res.json(401, {err: 'Not save'});
 					var d=sails.hooks.email.send(
 						"emailTemplates",
 						{
 							Name: email.name,
 							Url:"http://localhost:1337/auth/recoveryPass?sec="+email.token_unique+"&email="+email.to
 						},
 						{
 							to: email.to,
 							subject: "Sistema de Recuperacao"

 						},
 						function(err) {
 							sails.log(err);
 						}
 						);
 					sails.log(d);
 					return res.json(200, {data: {status:true}});
 				});
 			}else{
 				res.json(403, {err: 'user bloqued'});
 			}
 		});
 		

 	}
 };

