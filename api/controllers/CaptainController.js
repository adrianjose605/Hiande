/**
 * CapitainController
 *
 * @description :: Server-side logic for managing capitains
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {
 	view: function(req, res){
 		if(req.cookies.token){
 			Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
 				if(result.salescaptain){
 					return res.view('captain/captain',{'profile':result});
 				}else
 				return res.redirect('login');
 				
 			});
 		}
 		else
 			return res.redirect('login');

 	}
 };

