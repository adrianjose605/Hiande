/**
 * ShipController
 *
 * @description :: Server-side logic for managing ships
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

 		ships: function (req, res) {		
		if(req.cookies.token){
			  Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
        if(result){
          if(result.ships)
			return res.view('ship/ship',{'profile':result});
          else
            return res.redirect('/');    
        }
      });
		}
		else
			return res.redirect('/login')
	},
	getShip:function(req,res){

		
		if(req.cookies.token){
			  User.findOne(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
			  	
			  	if(result && result.idprofile==1)
			  		Ship.find({idcaptain:result.id},function(err,resu){
			  			res.json(resu);

			  		});
			  	else if(result)
			  		Ship.find({},function(err,resu){
			  			res.json(resu);
			  		});


			  });
			}
	}

 };

