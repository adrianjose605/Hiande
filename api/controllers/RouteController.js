/**
 * RouteController
 *
 * @description :: Server-side logic for managing routes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

 	saveRouteDest:function(req,res){

 		var r=req.param('idRoute');
 		var d=req.param('idDest');
 		if(r && d)
 			Route.findOne(r).exec(function(err, route) {

 				if(err) {
 					sails.log("err");
 					return res.send(err);
  				}// handle error
  			
  			route.destinations.add(d);
  			var rep="";  			
  			route.save(function(err) {
  				rep={'err':"error repetido"};
  				//return res.json({'err':400});
  				
  			});
  			if(rep!="")
  			return res.json({'status':405});
  			else
  			return res.json({'status':200});


  		});
 //return res.json({error:'Not find params'});
}
};

