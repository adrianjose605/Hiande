/**
 * PassengerController
 *
 * @description :: Server-side logic for managing passengers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

	view: function(req, res){
 		if(req.cookies.token){
 			Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
 				if(result.passenger){
 					return res.view('passenger/passenger',{'profile':result});
 				}else
 				return res.redirect('login');
 				
 			});
 		}
 		else
 			return res.redirect('login');

 	},
 	findByName: function(req, res) {
 		
 		var name = req.param('name');
 		
 		Passenger.findName(name,function (err, passengers) {
 			if (err) {
 				res.send(400);
 			} else {
 				res.send(passengers);
 			}
 		});
 	},

 };

