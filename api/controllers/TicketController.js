/**
 * TicketController
 *
 * @description :: Server-side logic for managing tickets
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {
 	view: function(req, res){
 		if(req.cookies.token){
 			Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
 				if(result){
 					setTimeout(function(){ 

 					if(result.ticket)
 						return res.view('sales/ticket',{'profile':result});
 					else
 						return res.redirect('/');    
 					}, 0);
 				}
 			});
 		}
 		else
 			return res.redirect('login');

 	},
 	cantticket:function(req,res){
 		var param=req.allParams()
 		Ticket.countTicket({'idship':param.idship,'idroute':param.idroute,'date_travel':param.date_travel,'type_passagem':1,'status':true},function(err,cant_g){
 			Ticket.countTicket({'idship':param.idship,'idroute':param.idroute,'date_travel':param.date_travel,'type_passagem':2,'status':true},function(err,cant_c){
 				Ticket.countTicket({'idship':param.idship,'idroute':param.idroute,'date_travel':param.date_travel,'type_passagem':3,'status':true},function(err,cant_s){
 					if(err)
 						res.send(err);

 					res.json({'cant_g':cant_g,'cant_c':cant_c,'cant_s':cant_s});
 				});
 			});
 		});
 	},
 	report:function(req,res){
 		var param=req.allParams();
 		//sails.log(param);
 		var where={};
 		var dateEnd=new Date(param.date_f);
 		dateEnd.setHours(24);
 		if(param.date_i && param.date_f){
 			where.createdAt={'>=':new Date(param.date_i).toISOString(),'<=':dateEnd.toISOString()};
 		}
 		where.status=true;
 		if(param.status==false)
 			where.status=false;

 		if(param.ship && param.ship!='undefined'){where.idship=param.ship};
 		if(param.route && param.route!='undefined'){where.idroute=param.route};
 		
 		Ticket.find(where).populate('idpassenger').exec(function(err,resp){
 			if(err)
 				return res.send(err);
 			
 			for (var i = 0; i < resp.length; i++) {
 				resp[i].createdAt=(new Date(resp[i].createdAt).getDate()<10?'0':'')+""+new Date(resp[i].createdAt).getDate()+"/"+(new Date(resp[i].createdAt).getMonth()<10?'0':'')+""+(+new Date(resp[i].createdAt).getMonth()+1)+"/"+new Date(resp[i].createdAt).getFullYear();

 				if(i==0)
 					resp[i].total=resp[i].coste;
 				else
 					resp[i].total=resp[i-1].total+resp[i].coste;
 				if(resp[i].type_passagem==1){resp[i].type_passagem="Economica";}
 				if(resp[i].type_passagem==2){resp[i].type_passagem="Camarote";}
 				if(resp[i].type_passagem==3){resp[i].type_passagem="Suit";}

 			}
 			res.send(resp);
 		});

 	}


 };

