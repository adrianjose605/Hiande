/**
 * ReportController
 *
 * @description :: Server-side logic for managing reports
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	view: function(req, res){
 		if(req.cookies.token){
 			Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
 				if(result.reports){
 					return res.view('report/main',{'profile':result});
 				}else
 				return res.redirect('login');
 				
 			});
 		}
 		else
 			return res.redirect('login');

 	}
};

