/**
 * ServiceController
 *
 * @description :: Server-side logic for managing services
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

 	sales: function (req, res) {	
 		if(req.cookies.token){
 			Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
 				if(result){
 					if(result.sales)
 						return res.view('sales/sales',{'profile':result});
 					else
 						return res.redirect('/');    
 				}
 			});
 		}
 		else
 			return res.redirect('/login')
 	},
	dashboard: function (req, res) {		
		if(req.cookies.token){
			Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
				
				if(result){
					if(result.sales)
						return res.view('dashboard/main',{'profile':result});
					else
						return res.redirect('/');    
				}
			});
		}
		else
			return res.redirect('/login')
	},
	sale: function (req, res) {		
		if(req.cookies.token){
			Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
				if(result){
					if(result.sales)
						return res.view('sales/sales',{'profile':result});
					else
						return res.redirect('/');    
				}
			});
		}
		else
			return res.redirect('/login')
	}
	
};

