/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 module.exports = {
  index: function (req, res) {
    var username = req.param('username');
    var password = req.param('password');

    if (!username || !password) {
      return res.json(401, {err: 'username and password required'});
    }

    User.findOne({username: username}, function (err, user) {
      if (!user) {
        return res.json(401, {err: 'invalid username or password'});
      }
      if(user.status)
        User.comparePassword(password, user, function (err, valid) {
          if (err) {
            return res.json(403, {err: 'forbidden'});
          }

          if (!valid) {
            return res.json(401, {err: 'invalid username or password'});
          } else {
            res.json({
              user: user,
              token: jwToken.issue({id : user.id })
            });
          }
        });
      else{
        res.json(403, {err: 'user bloqued'});
      }
    })
  },

  recoveryPass: function(req, res){
   var email = req.param('email');
   var security = req.param('sec');
   
   if (!email)
    return res.json(401, {err: 'email required'});
    if(!security)
    return res.json(401, {err: 'token required'});


  User.findOne({email: email}, function (err, user) {
    if (!user) {
      return res.json(401, {err: 'invalid email'});
    }
    if(user.status){
      Email.findOne({to:email,token_unique:security},function(err,emailF){
        if (!emailF) 
          return res.json(400, {err: 'recovery not found'});
        
        if(emailF.valid>=new Date())
        User.update({id:user.id},{password:'hiande'}).exec(function (err, updated){
            res.view('emailTemplates/PasswordSuccess',{'obj':{'name':updated[0].name,'username':updated[0].username}});
        });
        else
            res.view('emailTemplates/LinkExpired',{'obj':{'name':user.name,'username':user.username}});
      });
    }else{
       res.view('emailTemplates/PasswordLocked',{'obj':{'name':user.name,'username':user.username}});
    }
  });
},

logout: function(req, res){
  res.clearCookie('token');
  res.clearCookie('name');
  res.clearCookie('permission');
  return res.redirect('/login');
},

signup: function(req, res){
  if(req.cookies.token)
    return res.view('register');
  else
    return res.redirect('login');
},

changePass: function(req, res){
  return res.redirect('login');
},

userAll: function(req, res){
  if(req.cookies.token){
   Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
    if(result){
      if(result.viewusers)
        return res.view('user/user',{'profile':result});
      else
        return res.redirect('/');    
    }
  });

 }
 else 
  return res.view('login');  
},

profileAll: function(req, res){
  if(req.cookies.token){
      //sails.log(jwToken.verify(req.cookies.token.replace(/"/g,'')).id);
      Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
        if(result){
          if(result.viewprofile)
            return res.view('profile/profile',{'profile':result});
          else
            return res.redirect('/');    
        }
      });
    }
    else 
      return res.view('login');    
  },
  myaccount: function(req, res){
    if(req.cookies.token)
      Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
        if(result){
          if(result.viewmyaccount)
            return res.view('user/account',{'profile':result});
          else
            return res.redirect('/');    
        }
      });

    else 
      return res.view('login');    
  }

};
