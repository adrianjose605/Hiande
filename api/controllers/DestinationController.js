/**
 * DestinationController
 *
 * @description :: Server-side logic for managing destinations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 module.exports = {

 	city: function(req, res){

 		if(req.cookies.token){
 			Profile.verifyPrivileges(jwToken.verify(req.cookies.token.replace(/"/g,'')).id,function (err, result) {
 				if(result){
 					if(result.consultroute)
 						return res.view('linhas/destination',{'profile':result});
 					else
 						return res.redirect('/');    
 				}
 			});
 		}
 		else
 			return res.redirect('login');


 	},
 	// getAll(req,res){
 	// 	Destination.find().exec(function (err, data){
 	// 	  if (err) {
 	// 	    return res.serverError(err);
 	// 	  }
 	// 	  return res.json(data);
 	// 	});
 	// },
 	insertDestination(req,res){
 		var data=req.body.data;
 		
 		if(data.estados && data.estados.length>0){

 		for (var i =0; i< data.estados.length; i++) {
 			for (var j =0; j< data.estados[i].cidades.length; j++){
 				Destination.create({'status':true,'description':data.estados[i].cidades[j],'state':data.estados[i].nome,'abrvstate':data.estados[i].sigla}).exec(function (err, info) {
 					if(err) return res.json(err.status, {err: err});
 					//sails.log(info);
 					if(info)sails.log("tudo bom");//res.json(200, {data:"success"});
 				})
 			}
 		}
 	}else
 			res.json(400, {data:"json not found"});
 	}

 };

