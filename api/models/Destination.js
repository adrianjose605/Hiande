/**
 * Destination.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	status:{
  		type:'boolean',
  		defaultsTo:true
  	},
  	description:{
  		type:'string',
  		required:true,
      
  	},
    state:{
      type:'string',
      required:true
    },
      
  	abrvstate:{
  		type:'string',
  		required:true
  	}

    //  routes: {
    //   collection: 'route',
    //   via: 'destinations'
    // }
  }
};


