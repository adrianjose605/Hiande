/**
 * Capitain.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

  	status:{
  		type:'string',
  		enum:['Active', 'Inactive'],
  		defaultsTo: 'Active'
  	},
  	name:{
  		type:'string',
  		required:true
  	},
  	indentified:{
  		type:'string',
  		required:true
  	},
  	phone:{
  		type:'number'
  	}
  }
};

