/**
 * Ship.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    idcaptain:{
      model:'captain'
    },
  	status:{
  		type:'boolean',
  		defaultsTo: true
  	},
  	name:{
  		type:'string',
  		required:true
  	},
   status_generic:{
    type:'boolean',
    defaultsTo:false
   },
   status_cabins:{
    type:'boolean',
    defaultsTo:false
   },
   status_suit:{
    type:'boolean',
    defaultsTo:false
   },
   cant_generic:{
    type:'integer',
    defaultsTo:0
   },
   cant_cabins:{
    type:'integer',
    defaultsTo:0
   },
   cant_suit:{
    type:'integer',
    defaultsTo:0
   }

  	 // routes: {
    //   collection: 'route',
    //   via: 'ships'
    // }

  }
};

