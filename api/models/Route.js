/**
 * Route.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

  	status:{
  		type:'boolean',
  		defaultsTo: true,
  	},description:{
  		type:'string',
  		required:true
  	},type:{
  		type:'string',  		
  	}

  	 // destinations: {
    //   collection: 'destination',
    //   via: 'routes',
    //   dominant: true
    // }
    // ,
    // ships:{
    // 	model:'ship'
    // }

  }
};

