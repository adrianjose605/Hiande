/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var bcrypt = require('bcrypt');
module.exports = {

  attributes: {
  	status:{
  		type:'boolean',
  		defaultsTo: true
  	},
    name:{
      type:'string',
      required:true
    },
    idprofile:{
      model:"profile",
      required:true
    },
    idcompany:{
      model:"company",
      required:true
    },
    email:{
      type:'string'

    },
    username:{
  		type:'string',
  		required:true
  	},
    password:{
  		type:'string',
  		required:true
  	},
    toJSON: function () {
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }

  },



  beforeCreate : function (values, next) {
    bcrypt.genSalt(10, function (err, salt) {
      if(err) return next(err);
      bcrypt.hash(values.password, salt, function (err, hash) {
        if(err) return next(err);
        values.password = hash;
        next();
      })
    })
  },

  beforeUpdate : function (values, next) {
    if(values.password)
    bcrypt.genSalt(10, function (err, salt) {
      if(err) return next(err);
      bcrypt.hash(values.password, salt, function (err, hash) {
        if(err) return next(err);
        values.password = hash;
        next();
      })
    })
    else
      next();

  },


  comparePassword : function (password, user, cb) {
    bcrypt.compare(password, user.password, function (err, match) {

      if(err) cb(err);
      if(match) {
        cb(null, true);
      } else {
        cb(err);
      }
    })
  },
  resetPassword : function (password, user, cb) {
    
  },




   /**
   * Check validness of a login using the provided inputs.
   * But encrypt the password first.
   *
   * @param  {Object}   inputs
   *                     • username    {String}
   *                     • password {String}
   * @param  {Function} cb
   */
 // signup: function (inputs, cb) {
 //    // Create a user
 //    User.create({
 //      name: inputs.name,
 //      username: inputs.username,
 //      // TODO: But encrypt the password first
 //      password: inputs.password
 //    })
 //    .exec(cb);
 //  },
 //  attemptLogin: function (inputs, cb) {
    
 //    User.find({
 //      username: inputs.username,
 //      password: inputs.password
 //    })
 //    .exec(cb);
 //  }


};
