/**
 * Ship_dates.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 module.exports = {

 	attributes: {
 		idship:{
 			model:'ship',
 			required:true
 		},
 		idroute:{
 			model:'route',
 			required:true
 		},
 		status:{
            type:'boolean',
            defaultsTo:true,
            required:true
        },
        status_g:{
            type:'boolean',
            defaultsTo:true,
            required:true
        },
        status_c:{
            type:'boolean',
            defaultsTo:true,
            required:true
        },
        status_s:{
 			type:'boolean',
 			defaultsTo:true,
 			required:true
 		},
        date:{
            type:'date',
            required:true
        }, 
        date_return:{
            type:'date'
        },
 	    disp_generic:{
            type:'integer',
            defaultsTo:0
        },
        disp_cabins:{
            type:'integer',
            defaultsTo:0
        },
        disp_suit:{
            type:'integer',
            defaultsTo:0
        },
    },
    createOrUpdate:function(data,cb){
    var self = this; // reference for use by callbacks
        // If no values were specified, use criteria
        //if (!values) values = criteria.where ? criteria.where : criteria;
        //var findData={'idroute':data.idroute,'idship':data.idship,'iddest':data.iddest};
        this.findOne(data).then(function (result){
        	if(result){
        		self.update({'id':result.id}, data).exec(function afterwards(err, updated){
        			if (err) {
        				return;
        			}
        			return cb(null,updated);
        		});
        	}else{
        		self.create(data).exec(function (err, records) {
        			if(err)
        				return;
        			return cb(null,records);
        		});
        	}
        });

    },

    afterCreate:function(data,cb){
        var self=this;
        
        Ship.findOne({'id':data.idship}).then(function (result){
            if(result)
            self.update({'id':data.id},{'disp_generic':result.cant_generic,'disp_cabins':result.cant_cabins,'disp_suit':result.cant_suit}).exec(function(err,updated){
                cb();
            });
        });
        

    }

};

