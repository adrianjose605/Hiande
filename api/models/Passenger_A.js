/**
 * Passenger.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 module.exports = {

//  connection: 'agencia2',
  attributes: {
  	status:{
  		type:'boolean',
  		defaultsTo: true
  	},
  	name:{
  		type:'string',
  		required:true
  	},
  	birthdate:{
  		type:'date',
  		// required:true
  	},
    identified:{
      type:'string'
      
    },
  	cpf:{
  		type:'string'
  		
  	},
  	phone:{
  		type:'string',
  		required:true
  	},
  	address:{
  		type:'string'
  	},
  	email:{
  		type:'string'
  	}, 
  	
  },

  findName: function (option,cb) {
    name= '%'+option+'%';
    Passenger.query('SELECT id,name,email,identified,cpf,phone,address,birthdate FROM passenger WHERE name ILIKE $1', [ name ] ,function(err, rawResult) {
      if (err) { return cb(err); }

   // sails.log(rawResult);
  // (result format depends on the SQL query that was passed in, and the adapter you're using)

  // Then parse the raw result and do whatever you like with it.

  return  cb(null, rawResult.rows);

});
   // Passenger.findByName(option).exec(function (err, pass) {
   //  if (err) return cb(err);
   //  return  cb(null, pass);
   //  });
 }
};

