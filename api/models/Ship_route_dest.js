/**
 * Ship_route_dest.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 module.exports = {



  attributes: {

  	idship:{
  		model:'ship'
  	},
    idroute:{
  		model:'route'
  	},
    iddest:{
  		model:'destination'
  	},
  	status:{
  		type:'boolean',
  		defaultsTo:true,
  		required:true
  	},
    nroorden:{
      type:'integer',
      required:true
    },
    costo_generic:{
      type:'float',
      defaultsTo:0
    },
    costo_cabins:{
      type:'float',
      defaultsTo:0
    },
    costo_suit:{
      type:'float',
      defaultsTo:0
    },
    time:{
      type:'string'
      
    },
    time_return:{
            type:'string'
    }
    
},
  createOrUpdate:function(data,cb){
    var self = this; // reference for use by callbacks
        // If no values were specified, use criteria
        //if (!values) values = criteria.where ? criteria.where : criteria;
        var findData={'idroute':data.idroute,'idship':data.idship,'iddest':data.iddest};
        this.findOne(findData).then(function (result){
          if(result){
            self.update({'id':result.id}, data).exec(function afterwards(err, updated){
             if (err) {
              return;
            }
            return cb(null,updated);
          });
          }else{
            self.create(data).exec(function (err, records) {
              if(err)
                return;
              return cb(null,records);
            });
          }
        });

      },
      findAllRoute: function (option,cb) {
        name= option;
        Ship_route_dest.query('SELECT idroute FROM ship_route_dest group by idroute order by idroute ASC',function(err, rawResult) {

          if (err) return cb(null,err);

          if (!rawResult) return cb(null,new Error('Routes not find'));

          return  cb(null, rawResult.rows);

        });
      },

      findShip_R: function (option,cb) {
        route=option;
        Ship_route_dest.query('SELECT ship_route_dest.idship FROM ship_route_dest WHERE ship_route_dest.idroute=$1 group by idship order by idship ASC', [ route ] ,function(err, rawResult) {

          if (err) return cb(null,err);

          if (!rawResult) return cb(null,new Error('Ship not find'));

          return  cb(null, rawResult.rows);

        });
      },
      findShip_S: function (option,cb) {
        ship=option;
        Ship_route_dest.query('SELECT ship_route_dest.idroute FROM ship_route_dest WHERE ship_route_dest.idship=$1 group by idroute order by idroute ASC', [ ship ] ,function(err, rawResult) {

          if (err) return cb(null,err);

          if (!rawResult) return cb(null,new Error('Ship not find'));

          return  cb(null, rawResult.rows);

        });
      },

      findDestination_RS: function (option,cb) {
        var route=option[0];
        var ship=option[1];
        Ship_route_dest.query('SELECT ship_route_dest.id, ship_route_dest.iddest, destination.description, ship_route_dest.time, ship_route_dest.time_return, destination.abrvstate, ship_route_dest.costo_generic as generic,ship_route_dest.costo_cabins as cabins,ship_route_dest.costo_suit as suit  FROM ship_route_dest , destination  WHERE ship_route_dest.idroute=$1 and ship_route_dest.idship=$2 and destination.id=ship_route_dest.iddest and ship_route_dest.status=true order by ship_route_dest.nroorden ASC', [route,ship] ,function(err, rawResult) {

          if (err) return cb(null,err);

          if (!rawResult) return cb(null,new Error('Ship not find'));

          return  cb(null, rawResult.rows);

        });
      },

      destroy_res: function (option,cb) {
       var url="DELETE FROM ship_route_dest WHERE ship_route_dest.idship=$1 and ship_route_dest.idroute=$2";
       for (var i= 3; i < (option[1]*1)+3; i++) {
        if(i==0)
          url=url+' AND ship_route_dest.id!=$'+i;
        else
          url=url+' AND ship_route_dest.id!=$'+i;
      }
      
      sails.log(url);
      sails.log(option);

      
      if((option[1]*1)>0)
        Ship_route_dest.query(url, _.values(option[0]),function(err, rawResult) {
         // sails.log(err);
          
          if (err) return cb(null,err);

          if (!rawResult) return cb(null,new Error('Ship not find'));

          return  cb(null, rawResult);

        });
    }



  };

