/**
 * Service.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	status:{
  		type:'string',
  		enum:['Active', 'Inactive'],
  		defaultsTo: 'Active'
  	},
  	description:{
  		type:'string',
  		required:true
  	},
  	idPassenger:{
  		type:'number',
  		required:true
  	},
  	idRoute:{
  		type:'number',
  		required:true
  	},
  	idShip:{
  		type:'number',
  		required:true
  	},
  	idPromotion:{
  		type:'number'

  	}
  }
};

