/**
 * Profile.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 module.exports = {

  attributes: {
  	status:{
  		type:'boolean',
  		defaultsTo: true
  	},
    description:{
      type:'string',
      required:true
    },
    viewdashboard:{
      type:'boolean',
      defaultsTo:false
    },
    salescaptain:{
      type:'boolean',
      defaultsTo:false
    },
    viewusers:{
      type:'boolean',
      defaultsTo:false
    },
    changeusers:{
      type:'boolean',
      defaultsTo:false
    },
    viewprofile:{
      type:'boolean',
      defaultsTo:false
    },
    changeprofile:{
      type:'boolean',
      defaultsTo:false
    },
    viewmyaccount:{
      type:'boolean',
      defaultsTo:false
    },
    changemyaccount:{
      type:'boolean',
      defaultsTo:false
    },
    changepassword:{
      type:'boolean',
      defaultsTo:false
    },
    recoverypassword:{
      type:'boolean',
      defaultsTo:false
    },
    sales:{
      type:'boolean',
      defaultsTo:false
    },
    routes:{
      type:'boolean',
      defaultsTo:false
    },
    ships:{
      type:'boolean',
      defaultsTo:false
    },
    passenger:{
      type:'boolean',
      defaultsTo:false
    },
    reports:{
      type:'boolean',
      defaultsTo:false
    },
    ticket:{
      type:'boolean',
      defaultsTo:false
    },
    newticket:{
      type:'boolean',
      defaultsTo:false
    },
    consultticket:{
      type:'boolean',
      defaultsTo:false
    },
    newpackagetourism:{
      type:'boolean',
      defaultsTo:false
    },
    consultpackagetourism:{
      type:'boolean',
      defaultsTo:false
    },
    newroute:{
      type:'boolean',
      defaultsTo:false
    },
    consultroute:{
      type:'boolean',
      defaultsTo:false
    },
    form:{
      type:'boolean',
      defaultsTo:false
    },
    form_dscto:{
      type:'boolean',
      defaultsTo:false
    },

  },

  verifyPrivileges:function(option,cb){

    User.findOne({'id':option}).then(function (result){
      if(result){
        Profile.findOne({'id':result.idprofile}).then(function(res){
          if(res)
            cb(null,res);            
        });
      }
    });


  }
};

