/**
 * Ticket.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 module.exports = {

 	attributes: {

 		idship:{
 			model:'ship',
 			required:true
 		},
 		idroute:{
 			model:'route',
 			required:true
 		},
 		idpassenger:{
 			model:'passenger',
 			required:true
 		},
 		iddestinationorigin:{
 			model:'destination',
 			required:true
 		},
 		iddestinationend:{
 			model:'destination',
 			required:true
 		},
 		date_travel:{
      type:'date',
      required:true
    },
    date_travel_end:{
      type:'date',
      required:true
    },
    time_travel:{
      type:'text'
    },
    time_travel_end:{
      type:'text'
    },
    type_passagem:{
      type:'integer',
      required:true
    },
    coste:{
      type:'float',
      required:true
    },
    tax:{
      type:'float',
      defaultsTo:0
    },
    other:{
      type:'float',
      defaultsTo:0
    },
    safe:{
      type:'float',
      defaultsTo:0
    },
    cost_total:{
      type:'float',
      required:true
    },
    rate_discount:{
      type:'float'
    },
    status:{
      type:'boolean',
      required:true,
      defaultsTo:true
    },
    cod_ticket:{
      type:'text',
      defaultsTo:'00-000000'
    },
    idseller:{
      model:'user'

    }

  },
  restar_one:function(param,type){
    Ship_dates.findOne({ where: param}).exec(function(err,find){
      var values={};
      if(type=='1'){
        values={disp_generic:(find.disp_generic*1)-1};
      }else
      if(type=='2'){
        values={disp_cabins:(find.disp_cabins*1)-1};
      }else
      if(type=='3'){
        values={disp_suit:(find.disp_suit*1)-1};
      }

      Ship_dates.update(param,values).exec(function(err,updated){
        if(err)
          cb(err)
        return;
      });

    });
  },
  afterCreate: function (values, cb) {

   var cod=Ticket.return_cero(values.idship.toString().length,2)+values.idship+'-'+Ticket.return_cero(values.id.toString().length,6)+values.id;
       //insertar codigo de vendedor
       Ticket.update({id:values.id},{cod_ticket:cod}).exec(function afterwards(err, updated){
        if(err)
         cb(err);
       Ticket.countTicket({'type_passagem':1,'idship':values.idship,'idroute':values.idroute,'date_travel':values.date_travel,'status':true},function(err,cant_g){
         Ticket.countTicket({'type_passagem':2,'idship':values.idship,'idroute':values.idroute,'date_travel':values.date_travel,'status':true},function(err,cant_c){
           Ticket.countTicket({'type_passagem':3,'idship':values.idship,'idroute':values.idroute,'date_travel':values.date_travel,'status':true},function(err,cant_s){
            sails.sockets.blast("cantSold"+values.idroute+""+values.idship+""+new Date(values.date_travel).toISOString(), {"cant_g":cant_g,"cant_c":cant_c,"cant_s":cant_s}); 
          });
         });
       });
       Ticket.restar_one({idroute:values.idroute,idship:values.idship,date_return:values.date_travel_end,date:values.date_travel},values.type_passagem);
       return;
     });

       cb();
     },
     beforeCreate:function (values, next){
      // if(jwToken.verify(req.cookies.token.replace(/"/g,'')).id){
      //  values.idcompany=jwToken.verify(req.cookies.token.replace(/"/g,'')).id;

      // }
      next();

    },
    return_cero:function(length,max){

     var cero="";
     for (var i = 0; i < max-length; i++) {

      cero=cero+"0";
    }
    return cero;
  },
  beforeValidate: function(values, cb){

   Ship_dates.findOne({ where: { 'idship': values.idship, 'idroute':values.idroute, 'date': values.date_travel, 'date_return':values.date_travel_end}}).exec(function(err,dataResponse){

    if((values.type_passagem==1 && dataResponse) && (dataResponse.disp_generic<1 || dataResponse.status_g==false)){     		
     cb("Error, no lugar disponible"); 
   }else 	if((values.type_passagem==2 && dataResponse) && (dataResponse.disp_cabins<1 || dataResponse.status_==false)){

     cb("Error, no lugar disponible"); 
   }else  if((values.type_passagem==3 && dataResponse) && (dataResponse.disp_suitc<1 || dataResponse.status==false)){
     cb("Error, no lugar disponible"); 

   }else{
     cb();     		
   }

 });

 },
 countTicket:function(values,cb){
  Ticket.count(values).exec(function(err,found){
   if(err)
    cb(err);
  if(found)
    cb(null,found);
  else
    cb(null,0);

});
}
};

